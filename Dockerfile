FROM mcr.microsoft.com/dotnet/runtime:5.0-focal
RUN apt update && apt install -y make git cpanminus && cpanm JSON::Tiny
WORKDIR /work
ADD release/ntx /run
ADD scripts/ntx /bin/ntx
RUN chmod a+x /bin/ntx
ENTRYPOINT ["dotnet", "/run/ntx.dll"]


