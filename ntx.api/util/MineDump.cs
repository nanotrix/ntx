﻿using cz.ntx.proto.v2t.misc;
using ntx.api.pipe.events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Linq;

namespace ntx.api.util
{
    public static class MineDump
    {
        private static int FindNextStart(this List<Alignment> list, int start)
        {
            int ret = -1;

            for(var i = start; i< list.Count-1; i++)
            {
                if (list[i-1].AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp
                    && list[i].AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp
                    && list[i+1].AlignmentCase == Alignment.AlignmentOneofCase.AlignedLabel
                    )
                {
                    ret = i;
                    break;
                }
            }

            return ret;
        }

        private static int FindNextEnd(this List<Alignment> list, int start)
        {
            int ret = -1;

            for (var i = start; i < list.Count - 1; i++)
            {
                if (list[i - 1].AlignmentCase == Alignment.AlignmentOneofCase.AlignedLabel
                    && list[i].AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp
                    && list[i + 1].AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp
                    )
                {
                    ret = i;
                    break;
                }
            }

            return ret;
        }

        public static Evaluation.Types.Item ConvertToMine(this Evaluation.Types.Item eval, TimeSpan minDuration, TimeSpan maxDuration)
        {

            var orig = new List<Alignment>();
            var flat = new List<Alignment>();
            foreach(var b in eval.Blocks)
            {
                foreach(var a in b.Alignment)
                {
                    if (orig.Count > 0
                        && a.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp
                        && a.AlignedTimeStamp.Timestamp.ValueCase == cz.ntx.proto.v2t.engine.Event.Types.Timestamp.ValueOneofCase.Timestamp_
                        && orig.Last().AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp
                        && orig.Last().AlignedTimeStamp.Timestamp.ValueCase == cz.ntx.proto.v2t.engine.Event.Types.Timestamp.ValueOneofCase.Timestamp_
                        && a.AlignedTimeStamp.Timestamp.Timestamp_ == orig.Last().AlignedTimeStamp.Timestamp.Timestamp_
                        )
                    {
                        continue;
                    }
                    else
                    {

                        orig.Add(a.Clone());
                    }


                    if (a.AlignmentCase == Alignment.AlignmentOneofCase.AlignedLabel) {

                        if(a.AlignedLabel.DoScoring)
                            flat.Add(a);
                        continue;
                    }
                    
                    if (a.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp
                        && a.AlignedTimeStamp.Timestamp.ValueCase == cz.ntx.proto.v2t.engine.Event.Types.Timestamp.ValueOneofCase.Timestamp_
                        )
                    {
                        var c = flat.Count;
                        if (flat.Count > 1 
                            && flat[c - 1].AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp
                            && flat[c - 2].AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp
                            )
                        {
                            flat[c - 1].AlignedTimeStamp.Timestamp.Timestamp_ = a.AlignedTimeStamp.Timestamp.Timestamp_;
                        }
                        else
                        {
                            flat.Add(a);
                        }
                        continue;
                    }
                }
            }

            

            var blocks =   new List<Tuple<TimeSpan, TimeSpan>>();
            int start = flat.FindNextStart(1); ;
            while (start > 0)
            {

                var startTime = TimeSpan.FromTicks((long)flat[start].AlignedTimeStamp.Timestamp.Timestamp_);
                var end = flat.FindNextEnd(start);
                if (end < 0)
                    break;
                var endTime = TimeSpan.FromTicks((long)flat[end].AlignedTimeStamp.Timestamp.Timestamp_);
                while (endTime - startTime < minDuration)
                {
                    end = flat.FindNextEnd(end+1);
                    if (end < 0)
                    {
                        break;
                    }
                    endTime = TimeSpan.FromTicks((long)flat[end].AlignedTimeStamp.Timestamp.Timestamp_);
                }
                if (end < 0)
                    break;
                if(endTime - startTime < maxDuration)
                {
                    start = flat.FindNextStart(end + 1);
                    blocks.Add(new Tuple<TimeSpan, TimeSpan>(startTime, endTime));

                }
                else
                {
                    start = flat.FindNextStart(start + 1);
                }
                
            }
            var e = new Evaluation.Types.Item();

            var lastIndex = orig.FindIndex(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp);

            foreach (var t in blocks)
            {
                var ss = orig.FindIndex(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp && x.AlignedTimeStamp.Timestamp.Timestamp_ == (ulong)t.Item1.Ticks);
                var ee = orig.FindIndex(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp && x.AlignedTimeStamp.Timestamp.Timestamp_ == (ulong)t.Item2.Ticks);

                if(ss != lastIndex)
                {
                    var p = new AlignBlock();
                    p.Alignment.AddRange(orig.GetRange(lastIndex, ss - lastIndex + 1));
                    var offset = orig[lastIndex].AlignedTimeStamp.Timestamp.Timestamp_;
                    p.GetResults(offset);
                    e.Blocks.Add(p);
                    p.BType = AlignBlock.Types.AlignBlockType.Missmatch;
                }
                lastIndex = ee;

                var b = new AlignBlock();
                b.Alignment.AddRange(orig.GetRange(ss, ee - ss + 1));
                b.GetResults((ulong)t.Item1.Ticks);
                e.Blocks.Add(b);
                b.BType = AlignBlock.Types.AlignBlockType.Match;
            }
            
            if(orig.Count > lastIndex + 1)
            {
                var p = new AlignBlock();
                p.Alignment.AddRange(orig.GetRange(lastIndex, orig.Count - lastIndex));
                var offset = orig[lastIndex].AlignedTimeStamp.Timestamp.Timestamp_;
                p.GetResults(offset);
                e.Blocks.Add(p);
                p.BType = AlignBlock.Types.AlignBlockType.Missmatch;
            }
            
            e.GetResults();
            e.Id = eval.Id;

            return e;

        }
    }
}

