﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ntx.api.util
{

    public static class VoiceprintUtils
    {
        public static Voiceprint MergeLogExp(this List<Voiceprint> vplist)
        {
            var dim = vplist.First().X.Length;
            var spk = vplist.First().SpkId;
            var ver = vplist.First().Version;
            var cnt = vplist.Count;
            double[] x_exp_sum = Enumerable.Repeat<double>(0.0,vplist.First().X.Length).ToArray();
            double xc_exp_sum = 0.0;
            foreach( var v in vplist)
            {
                for(int i = 0; i < dim; i++)
                {
                    x_exp_sum[i] += Math.Exp(v.X[i]);
                }
                xc_exp_sum += Math.Exp(v.Xc);
            }
            var x_new = x_exp_sum.Select(x => (float)Math.Log(x / cnt)).ToArray();
            var xc_new = (float)Math.Log(xc_exp_sum / cnt);

            return new Voiceprint("merged", spk, ver, x_new, xc_new);
        }

        public static Voiceprint Merge(this List<Voiceprint> vplist)
        {
            var dim = vplist.First().X.Length;
            var spk = vplist.First().SpkId;
            var ver = vplist.First().Version;
            var cnt = vplist.Count;
            double[] x_exp_sum = Enumerable.Repeat<double>(0.0, vplist.First().X.Length).ToArray();
            double xc_exp_sum = 0.0;
            foreach (var v in vplist)
            {
                for (int i = 0; i < dim; i++)
                {
                    x_exp_sum[i] +=v.X[i];
                }
                xc_exp_sum += v.Xc;
            }
            var x_new = x_exp_sum.Select(x => (float)(x / cnt)).ToArray();
            var xc_new = (float)(xc_exp_sum / cnt);

            return new Voiceprint("merged", spk, ver, x_new, xc_new);
        }

        public static List<Voiceprint> MergeBySpeakerId(this List<Voiceprint> enroll)
        {
            var grouped = enroll.GroupBy(x => x.SpkId);
            var ret = new List<Voiceprint>();
            foreach(var l in grouped)
            {
                ret.Add(l.ToList().Merge());
            }
            return ret;
        }

        public static Dasync.Collections.AsyncEnumerable<Voiceprint> CSVEnumerator(Stream stream,CancellationToken token = default(CancellationToken))
        {
            return new Dasync.Collections.AsyncEnumerable<Voiceprint>(async yield =>
            {

                using (var _stream = new StreamReader(stream, new UTF8Encoding()))
                {
                    while (true)
                    {
                        var line = await _stream.ReadLineAsync();
                        if (line == null)
                            break;
                        var s = line.Split(new char[] { '|' });
                        if (s.Length != 3)
                            continue;

                        await yield.ReturnAsync(new Voiceprint(s[0], s[1], s[2]));
                        if (token.IsCancellationRequested)
                            break;
                    }

                }
            });
        }

    }

        public class Voiceprint
    {
        public string UtteranceId { get; private set; }
        public string SpkId { get; private set; }

        public string VoicePrint { get; private set; }


        public string Version { get; private set; }
        public float[] X { get; private set; }
        public float Xc { get; private set; }



        public Voiceprint(string utteranceId, string spkId, string voiceprint)
        {
            UtteranceId = utteranceId;
            SpkId = spkId;
            var parts = voiceprint.Split(new char[] { '.'});
            Version = parts[0];
            var bytes = Convert.FromBase64String(parts[1]);
            X = new float[bytes.Length / 4];
            for (int i = 0; i < X.Length; i++)
            {
                X[i] = BitConverter.ToSingle(bytes, i * 4);
            }

            Xc = BitConverter.ToSingle(Convert.FromBase64String(parts[2]),0);

            VoicePrint = voiceprint;

        }

        public class ROCPoint
        {
            //false acceptance ratio
            public float FAR { get; set; }
            //false rejection ratio
            public float FRR { get; set; }
            //LogLikelihood ratio
            public float Threshold { get; set; }
        }

        public class VoceprintResult
        {
            public float LogLikelihood { get; set; }
            public Voiceprint VoicePrint1 { get; set; }
            public Voiceprint VoicePrint2 { get; set; }
        }

        public List<VoceprintResult> CompareWith(List<Voiceprint> enroll)
        {
            var ret = new List<VoceprintResult>();
            foreach(var v in enroll)
            {
                ret.Add(GetLogLikelihood(v));
            }

            return ret.OrderByDescending(x => x.LogLikelihood).ToList();
        }

        private VoceprintResult GetLogLikelihood(Voiceprint v)
        {
            float sum = Xc+v.Xc;
            for (int i=0; i< X.Length; i++)
            {
                sum += X[i] * v.X[i];
            }
            return new VoceprintResult { VoicePrint1 = this, VoicePrint2 = v,  LogLikelihood= sum };
        }

        public Voiceprint(string utteranceId, string spkId, string version, float[] x, float xc)
        {
            UtteranceId = utteranceId;
            SpkId = spkId;
            Version = version;

            X = x;
            Xc = xc;

            var bytes = new byte[X.Length * 4];
            for (int i = 0; i < X.Length; i++)
            {
                var b = BitConverter.GetBytes(X[i]);
                Array.Copy(b, 0, bytes, 4 * i, 4);
            }

            var v = Convert.ToBase64String(bytes, Base64FormattingOptions.None);
            var vc = Convert.ToBase64String(BitConverter.GetBytes(xc), Base64FormattingOptions.None);
            VoicePrint = Version + "." + v + "." + vc;
        }
    }
}
