﻿using cz.ntx.proto.v2t.engine;
using cz.ntx.proto.v2t.misc;
using ntx.api.pipe.events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ntx.api.util
{
    public static class EvalUtils
    {


        public static List<AlignBlock> ToBlocks(this List<Alignment> alignment)
        {

            var cblock = new AlignBlock() {
                BType = AlignBlock.Types.AlignBlockType.None,
            };
            cblock.Alignment.AddRange(alignment);
            return new List<AlignBlock>() { cblock };

        }


        public static List<AlignBlock> NoiseSplit(this List<AlignBlock> blocks, TimeSpan minDuration)
        {

            var ret = new List<AlignBlock>();
            
            foreach(var b in blocks)
            {
                if (b.BType == AlignBlock.Types.AlignBlockType.Ins)
                    ret.AddRange(b.NoiseSplit(minDuration));
                else
                    ret.Add(b);
            }
            return ret;
        }

        private static List<AlignBlock> MergeSame(this List<AlignBlock> blocks)
        {

            var ret = new List<AlignBlock>();

            
            foreach (var b in blocks)
            {
                if(ret.Count ==0 )
                {
                    ret.Add(b);
                    continue;
                }

                if(ret.Last().BType==b.BType)
                {
                    ret.Last().AppendAlignment(b);
                }
                else
                {
                    ret.Add(b);
                }
            }
            return ret;
        }

        private static void AppendAlignment(this AlignBlock block, AlignBlock next)
        {
            bool first = true;
            foreach (var a in next.Alignment)
            {
                if (first)
                {
                    first = false;
                    if (a.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp)
                        continue;
                }
                block.Alignment.Add(a);
            }
        }

        private static List<AlignBlock> NoiseSplit(this AlignBlock block, TimeSpan minDuration)
        {
            var ret = new List<AlignBlock>();
            var cblock = new AlignBlock() { BType = block.BType };
            foreach (var a in block.Alignment)
            {
                if(a.AlignmentCase != Alignment.AlignmentOneofCase.AlignedLabel || a.AlignedLabel.Res==null)
                {
                    cblock.Alignment.Add(a);
                    continue;
                }

                var cBlockType = block.BType;
                if(a.AlignedLabel.Res.LabelCase == Event.Types.Label.LabelOneofCase.Noise)
                    cBlockType = AlignBlock.Types.AlignBlockType.Noise;

                if (cblock.BType == cBlockType)
                {
                    cblock.Alignment.Add(a);
                    continue;
                }

                var prevEnd = cblock.Alignment.LastOrDefault(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp);
                var prevStart = cblock.Alignment.FirstOrDefault(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp);


                ulong duration = 0;
                if(prevEnd!=null && prevStart!=null)
                {
                    duration = prevEnd.AlignedTimeStamp.Timestamp.GetValue() - prevStart.AlignedTimeStamp.Timestamp.GetValue();
                }
                if (duration > (ulong)minDuration.Ticks)
                    cblock.BType = block.BType;

                ret.Add(cblock);
                cblock = new AlignBlock();
                if (prevEnd != null)
                    cblock.Alignment.Add(prevEnd);
                cblock.Alignment.Add(a);
                cblock.BType = cBlockType;
                
                
            }


            ret.Add(cblock);


            return ret;
        }

        public static List<AlignBlock> FineTune(this List<AlignBlock> blocks, TimeSpan maxDuration)
        {
            var ret = new List<AlignBlock>();

            foreach (var s in blocks)
            {
                ret.AddRange(s.FineTune(maxDuration));
            }
            return ret;

        }


        public static List<AlignBlock> EvalClustering(this List<AlignBlock> blocks, Dictionary<string, string> speakerMap)
        {
            var ref2res = new Dictionary<string, Dictionary<string,long>>();
            //relabel
            string cRef = null;
            string cRes = null;
            foreach (var s in blocks)
            {
                foreach (var a in s.Alignment)
                {
                    if(a.AlignmentCase == Alignment.AlignmentOneofCase.AlignedSpeakerTurn)
                    {
                        cRef = a.AlignedSpeakerTurn.Ref;
                        cRes = a.AlignedSpeakerTurn.Res;
                        continue;
                    }
                    if (cRef == null || cRes == null)
                        continue;

                    if (a.AlignmentCase != Alignment.AlignmentOneofCase.AlignedLabel)
                        continue;
                    if (!a.AlignedLabel.DoScoring)
                        continue;
                    if (a.AlignedLabel.AType == Alignment.Types.AlignType.Ins)
                        continue;
                    if (ref2res.ContainsKey(cRef))
                        if (ref2res[cRef].ContainsKey(cRes))
                            ref2res[cRef][cRes]++;
                        else
                            ref2res[cRef][cRes] = 1;
                    else
                        ref2res[cRef] = new Dictionary<string, long>() { { cRes, 1 } };
                }
            }

            var res2ref = new Dictionary<string, string>();
            var resCount = new Dictionary<string, long>();
            foreach (var kv in ref2res)
            {
                var refL = kv.Key;
                var resL = kv.Value.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
                var resLCount = kv.Value.Aggregate((l, r) => l.Value > r.Value ? l : r).Value;
                if (!resCount.ContainsKey(resL))
                {
                    resCount[resL] = resLCount;
                    res2ref[resL] = refL;
                }
                else
                {
                    if(resLCount > resCount[resL])
                    {
                        resCount[resL] = resLCount;
                        res2ref[resL] = refL;
                    }
                }
                
            }

            foreach(var kv in res2ref)
            {
                speakerMap.Add(kv.Value, kv.Key);
            }

            foreach (var s in blocks)
            {
                foreach (var a in s.Alignment)
                {
                    if (a.AlignmentCase != Alignment.AlignmentOneofCase.AlignedSpeakerTurn)
                        continue;

                    if (res2ref.ContainsKey(a.AlignedSpeakerTurn.Res)) {
                        var res = res2ref[a.AlignedSpeakerTurn.Res] ;
                        if (a.AlignedSpeakerTurn.AType == Alignment.Types.AlignType.Sub && res == a.AlignedSpeakerTurn.Ref)
                            a.AlignedSpeakerTurn.AType = Alignment.Types.AlignType.Hit;

                            
                    }
                }
            }

            return blocks;
        }

        public static List<AlignBlock> EvalSpeakerTurns(this List<AlignBlock> blocks, Dictionary<string, string> speakerMap)
        {
            var prefColor = "";
            var presColor = "";
            var refColor = "None";
            var resColor = "None";
            foreach (var s in blocks)
            {
               
                var ablock = new List<Alignment>();
                Alignment lastTurn = null;
                foreach (var a in s.Alignment)
               {
                    if (a.AlignmentCase != Alignment.AlignmentOneofCase.AlignedLabel)
                    {
                        ablock.Add(a);
                        continue;
                    }
                    
                    if(a.AlignedLabel.AType == Alignment.Types.AlignType.None)
                    {
                        if(a.AlignedLabel.Ref != null)
                        {
                            refColor = a.AlignedLabel.Ref.Speaker;
                        }
                        if (a.AlignedLabel.Res != null)
                        {
                            resColor = a.AlignedLabel.Res.Speaker;
                        }
                        continue;
                    }
                    
                    if (!a.AlignedLabel.DoScoring)
                    {
                        ablock.Add(a);
                        continue;
                    }


                    if (refColor != prefColor || resColor != presColor)
                    {
                        if (lastTurn == null)
                        {
                            var n = new Alignment()
                            {
                                AlignedSpeakerTurn = new Alignment.Types.AlignedSpeakerTurn() { AType = Alignment.Types.AlignType.Sub }
                            };
                            ablock.Add(n);
                            n.AlignedSpeakerTurn.Ref = refColor;
                            n.AlignedSpeakerTurn.Res = resColor;
                            //n.AlignedSpeakerTurn.ResId = resColor;


                            if (refColor != prefColor && resColor == presColor)
                            {
                                lastTurn = n;
                                n.AlignedSpeakerTurn.AType = Alignment.Types.AlignType.Del;
                            }

                            if (resColor != presColor && refColor == prefColor)
                            {
                                lastTurn = n;
                                n.AlignedSpeakerTurn.AType = Alignment.Types.AlignType.Ins;
                            }
                        }
                        else
                        {
                            if(lastTurn.AlignedSpeakerTurn.AType == Alignment.Types.AlignType.Ins)
                            {
                                lastTurn.AlignedSpeakerTurn.Ref = refColor;
                            }
                            if (lastTurn.AlignedSpeakerTurn.AType == Alignment.Types.AlignType.Del)
                            {
                                lastTurn.AlignedSpeakerTurn.Res = resColor;
                                //lastTurn.AlignedSpeakerTurn.ResId = resColor;
                            }
                            lastTurn.AlignedSpeakerTurn.AType = Alignment.Types.AlignType.Sub;
                            lastTurn = null;
                        }

                        if (refColor != prefColor)
                            prefColor = refColor;

                        if (resColor != presColor)
                            presColor = resColor;
                    }
                    ablock.Add(a);
                    if (!(a.AlignedLabel.AType == Alignment.Types.AlignType.Ins || a.AlignedLabel.AType == Alignment.Types.AlignType.Del))
                    {
                        lastTurn = null;
                    }

                }
               //repair start alignment

               s.Alignment.Clear();
               s.Alignment.AddRange(ablock);
            }
            return blocks.EvalClustering(speakerMap);

        }

        private static List<AlignBlock> FineTune(this AlignBlock block, TimeSpan maxDuration)
        {
            if (block.Alignment.Count == 0)
                return new List<AlignBlock>() { block };

            var ret = new List<AlignBlock>();



            var labels = block.Alignment.Count(
                x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedLabel && 
                x.AlignedLabel.DoScoring
                );
            var deletions = block.Alignment.Count(
                x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedLabel &&
                x.AlignedLabel.DoScoring &&
                x.AlignedLabel.AType== Alignment.Types.AlignType.Del

                );
            var insertions = block.Alignment.Count(
               x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedLabel &&
               x.AlignedLabel.DoScoring &&
               x.AlignedLabel.AType == Alignment.Types.AlignType.Ins
               );

            if (labels == deletions)
            {
                block.BType = AlignBlock.Types.AlignBlockType.Del;
                ret.Add(block);
                return ret;
            }

            if (labels == insertions)
            {
                block.BType = AlignBlock.Types.AlignBlockType.Ins;
                ret.Add(block);
                return ret;
            }


            AlignBlock leftBlock = null;
            AlignBlock mainBlock = null;
            AlignBlock rightBlock = null;


            var aligment = block.Alignment.ToList();
            var left = aligment.TakeWhile(x =>
                 x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp ||
                     x.AlignedLabel.AType == Alignment.Types.AlignType.Ins || 
                     !x.AlignedLabel.DoScoring
                );

            if (left.Count(x => x.AlignmentCase== Alignment.AlignmentOneofCase.AlignedLabel) >0)
            {
                leftBlock = new AlignBlock() { BType = AlignBlock.Types.AlignBlockType.Ins };
                leftBlock.Alignment.AddRange(left);
                if (leftBlock.Duration() > (ulong)maxDuration.Ticks)
                    aligment.RemoveRange(0, left.Count());
                else
                    leftBlock = null;
                
            }

            aligment.Reverse();

            var right= aligment.TakeWhile(x =>
                  x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp ||
                      x.AlignedLabel.AType == Alignment.Types.AlignType.Ins ||
                     !x.AlignedLabel.DoScoring
                );

            

            if (right.Count(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedLabel) > 0)
            {
                rightBlock = new AlignBlock() { BType = AlignBlock.Types.AlignBlockType.Ins };
                rightBlock.Alignment.AddRange(right.Reverse());
                if (rightBlock.Duration() > (ulong)maxDuration.Ticks)
                    aligment.RemoveRange(0, right.Count());
                else
                    rightBlock = null;
            }

            aligment.Reverse();

            if(aligment.Count()>0)
            {
                mainBlock= new AlignBlock() { BType = AlignBlock.Types.AlignBlockType.Match };
                mainBlock.Alignment.AddRange(aligment);
            }


            if (mainBlock != null)
            {
               
                if (leftBlock != null)
                {
                    var prevEnd = leftBlock.Alignment.LastOrDefault(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp);
                    if(prevEnd!=null)
                        mainBlock.Alignment.Insert(0, prevEnd);
                    ret.Add(leftBlock);
                    //copy timestamp from end

                }
                ret.Add(mainBlock);
                if (rightBlock != null)
                {
                    var nextStart = rightBlock.Alignment.FirstOrDefault(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp);
                    if (nextStart != null)
                        mainBlock.Alignment.Add(nextStart);
                    ret.Add(rightBlock);
                }
            }


            return ret;
        }
       

       

        private static ulong Duration(this AlignBlock block)
        {
            var prevEnd = block.Alignment.LastOrDefault(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp);
            var prevStart = block.Alignment.FirstOrDefault(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp);
            ulong duration = 0;
            if (prevEnd != null && prevStart != null)
            {
                duration = prevEnd.AlignedTimeStamp.Timestamp.GetValue() - prevStart.AlignedTimeStamp.Timestamp.GetValue();
            }
            return duration;
        }


        public static List<AlignBlock> Split(this AlignBlock block)
        {

            var ret = new List<AlignBlock>();
            
            var cblock = new AlignBlock() {  BType= block.BType};

            foreach (var a in block.Alignment)
            {

                if (a.AlignmentCase != Alignment.AlignmentOneofCase.AlignedLabel)
                {
                    cblock.Alignment.Add(a);
                    continue;
                }


                var cBlockType = AlignBlock.Types.AlignBlockType.None;

                if (a.AlignedLabel.DoScoring)
                {
                    switch (a.AlignedLabel.AType)
                    {
                        case Alignment.Types.AlignType.Sub:
                        case Alignment.Types.AlignType.Hit:
                            cBlockType = AlignBlock.Types.AlignBlockType.Match;
                            break;
                        case Alignment.Types.AlignType.Ins:
                            cBlockType = AlignBlock.Types.AlignBlockType.Ins;
                            break;
                        case Alignment.Types.AlignType.Del:
                            cBlockType = AlignBlock.Types.AlignBlockType.Del;
                            break;
                    }
                }else
                {
                    cblock.Alignment.Add(a);
                    continue;
                }

                


                if (cblock.BType == AlignBlock.Types.AlignBlockType.None)
                    cblock.BType = cBlockType;

                if(cblock.BType==cBlockType)
                {
                    cblock.Alignment.Add(a);
                    continue;
                }

                var prevTs = cblock.Alignment.LastOrDefault(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp);

                ret.Add(cblock);

                cblock = new AlignBlock();
                if (prevTs != null)
                    cblock.Alignment.Add(prevTs);
                cblock.Alignment.Add(a);
                cblock.BType = cBlockType;
                

            }
            ret.Add(cblock);
            return ret;
        }


        public static void GetResults(this Evaluation eval)
        {
            eval.Total = new Evaluation.Types.Item()
            {
                Id = "TOTAL",
                Score = new Score() { Count = 0, Deletions = 0, Hits = 0, Insertions = 0, Substitutions = 0 },
                Speed = new Speed() { ProcessingDuration = 0, StreamDuration = 0 },
                Dscore = new DiarScore() { Count = 0, Deletions = 0, Hits = 0, Insertions = 0, ClusterCount = 0, ClusterMatch = 0 }
            };
            foreach (var i in eval.Items)
            {
                if (i.Score != null)
                {
                    eval.Total.Score.Count += i.Score.Count;
                    eval.Total.Score.Substitutions += i.Score.Substitutions;
                    eval.Total.Score.Insertions += i.Score.Insertions;
                    eval.Total.Score.Hits += i.Score.Hits;
                    eval.Total.Score.Deletions += i.Score.Deletions;
                }
                if (i.Speed != null)
                {
                    eval.Total.Speed.ProcessingDuration += i.Speed.ProcessingDuration;
                    eval.Total.Speed.StreamDuration += i.Speed.StreamDuration;
                }
                if(i.Dscore != null)
                {
                    eval.Total.Dscore.Count += i.Dscore.Count;
                    eval.Total.Dscore.ClusterCount += i.Dscore.ClusterCount;
                    eval.Total.Dscore.ClusterMatch += i.Dscore.ClusterMatch;
                    eval.Total.Dscore.Insertions += i.Dscore.Insertions;
                    eval.Total.Dscore.Hits += i.Dscore.Hits;
                    eval.Total.Dscore.Deletions += i.Dscore.Deletions;
                }
            }

            eval.Total.Score.GetResults();
            eval.Total.Speed.GetResults();
            if (eval.Total.Dscore.ClusterCount > 0)
            {
                eval.Total.Dscore.GetDiarResults();
            }
            else
            {
                eval.Total.Dscore = null;
            }
        }

        public static void GetResults(this Speed speed)
        {
            if (speed.ProcessingDuration != 0)
            {
                speed.Factor = (float)((double)speed.StreamDuration / (double)speed.ProcessingDuration);
            }
        }

        public static void GetDiarResults(this Evaluation.Types.Item evalItem, Dictionary<string,string> speakerMap)
        {
            

            var score = new DiarScore
            {
                Count = 0,
                Deletions = 0,
                Insertions = 0,
                Hits = 0,
                ClusterCount = 0,
                ClusterMatch = 0
            };
            foreach(var kv in speakerMap)
            {
                score.SpeakerMap.Add(new DiarScore.Types.SpeakerMapItem { Ref = kv.Key, Res = kv.Value });
            }
            bool hasScore = false;
            string cRef = null;
            string cRes = null;

            foreach (var b in evalItem.Blocks)
            {

                var bScore = new DiarScore
                {
                    Count = 0,
                    Deletions = 0,
                    Insertions = 0,
                    Hits = 0,
                    ClusterCount = 0,
                    ClusterMatch = 0
                };
                foreach (var a in b.Alignment)
                {
                    if(a.AlignmentCase == Alignment.AlignmentOneofCase.AlignedSpeakerTurn) {
                        cRef = speakerMap.ContainsKey(a.AlignedSpeakerTurn.Ref) ? speakerMap[a.AlignedSpeakerTurn.Ref] : a.AlignedSpeakerTurn.Ref;
                        cRes = a.AlignedSpeakerTurn.Res;
                        switch (a.AlignedSpeakerTurn.AType)
                        {
                            case Alignment.Types.AlignType.Hit:
                            case Alignment.Types.AlignType.Sub:
                                bScore.Count++;
                                bScore.Hits++;
                                break;
                            case Alignment.Types.AlignType.Ins:
                                bScore.Insertions++;
                                break;
                            case Alignment.Types.AlignType.Del:
                                bScore.Count++;
                                bScore.Deletions++;
                                break;
                        }
                        continue;
                    }
                    if (a.AlignmentCase != Alignment.AlignmentOneofCase.AlignedLabel)
                        continue;
                    if (!a.AlignedLabel.DoScoring)
                        continue;
                    if (a.AlignedLabel.AType == Alignment.Types.AlignType.Ins)
                        continue;


                    bScore.ClusterCount++;
                    if (cRef == cRes)
                    {
                        bScore.ClusterMatch++;
                    }
                }
                bScore.GetDiarResults();
                if (bScore.ClusterCount > 0)
                    b.Dscore = bScore;

                if (b.Dscore != null)
                {
                    score.Count += b.Dscore.Count;
                    score.Insertions += b.Dscore.Insertions;
                    score.Hits += b.Dscore.Hits;
                    score.Deletions += b.Dscore.Deletions;
                    score.ClusterCount += b.Dscore.ClusterCount;
                    score.ClusterMatch += b.Dscore.ClusterMatch;
                    hasScore = true;
                }
            }
            if (!hasScore)
                return;
            score.GetDiarResults();
            evalItem.Dscore = score;
            

        }
        private static void GetDiarResults(this DiarScore score)
        {
            if (score.Count == 0)
            {
                score.Recall = 0.0f;
                score.Precission = 0.0f;
                score.Frate = 0.0f;
            }
            else
            {
                score.Recall = (float)((double)(score.Hits) / (double)score.Count);
                if ((score.Hits + score.Insertions) == 0)
                {
                    score.Precission = 0.0f;
                }
                else
                {
                    score.Precission = (float)((double)(score.Hits) / (double)((score.Hits) + score.Insertions));
                }
                if ((score.Recall + score.Precission) == 0)
                {
                    score.Frate = 0.0f;
                }
                else
                {
                    score.Frate = (2 * score.Recall * score.Precission) / (score.Recall + score.Precission);
                }
            }
            if(score.ClusterCount == 0)
            {
                score.ClusterPurity = 0.0f;
            }
            else
            {
                score.ClusterPurity = (float)((double)(score.ClusterMatch) / (double)score.ClusterCount);
            }

            
        }

        

        public static void GetResults(this Evaluation.Types.Item evalItem)
        {
            if (evalItem.Speed != null && evalItem.Speed.ProcessingDuration != 0)
            {
                evalItem.Speed.Factor = (float)((double)evalItem.Speed.StreamDuration / (double)evalItem.Speed.ProcessingDuration);
            }
            
            var score = new Score
            {
                Count = 0,
                Deletions = 0,
                Insertions = 0,
                Hits = 0,
                Substitutions = 0
            };
            bool hasScore = false;
            ulong offset = 0;
            
            foreach (var b in evalItem.Blocks)
            {
                if (offset == 0)
                {
                    if (b.Alignment.Count(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp) > 0)
                        offset = b.Alignment.First(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp).AlignedTimeStamp.Timestamp.GetValue();
                }

                b.GetResults(offset);
                if (b.Alignment.Count(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp) > 0)
                    offset = b.Alignment.Last(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp).AlignedTimeStamp.Timestamp.GetValue();
                if (b.Score!=null)
                {
                    score.Count += b.Score.Count;
                    score.Substitutions += b.Score.Substitutions;
                    score.Insertions += b.Score.Insertions;
                    score.Hits += b.Score.Hits;
                    score.Deletions += b.Score.Deletions;
                    hasScore = true;
                }
            }
            if (!hasScore)
                return;
            score.GetResults();

            evalItem.Score = score;
        }

        

        private static void GetResults(this Score score)
        {
            if(score.Count==0)
            {
                score.Accuracy = 0.0f;
                score.Correctness = 0.0f;
                return;
            }
                

            if (score.Hits < score.Insertions)
                score.Accuracy = 0.0f;
            else
                score.Accuracy = (float)(((double)(score.Hits - score.Insertions)) / (double)(score.Count));

            score.Correctness = (float)(((double)(score.Hits)) / (double)(score.Count));
        }

        private static bool CanGetScore(this AlignBlock block)
        {
            if (block.Alignment.Count == 0)
                return false;

            return block.Alignment.FirstOrDefault(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedLabel && x.AlignedLabel.DoScoring) != null;
        }

        private static bool CanGetScore(this Evaluation.Types.Item item)
        {
            if (item.Blocks.Count == 0)
                return false;

            return item.Blocks.FirstOrDefault(x => x.Score!=null) !=null;
        }


        public static void GetResults(this AlignBlock block, ulong offset)
        {

            var end = block.Alignment.LastOrDefault(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp);
            if (end != null)
            {
                block.Position = new AlignBlock.Types.Position() { Offset = offset };
                block.Position.Duration = end.AlignedTimeStamp.Timestamp.GetValue() - block.Position.Offset;
            }

            if (!block.CanGetScore())
                return;
            var score = new Score
            {
                Count = 0,
                Deletions = 0,
                Insertions = 0,
                Hits = 0,
                Substitutions = 0
            };


            foreach (var item in block.Alignment)
            {
                switch (item.AlignmentCase)
                {
                    case Alignment.AlignmentOneofCase.AlignedLabel:
                        if (!item.AlignedLabel.DoScoring)
                            continue;
                        switch (item.AlignedLabel.AType)
                        {
                            case Alignment.Types.AlignType.Hit:

                                score.Count++;
                                score.Hits++;
                                break;
                            case Alignment.Types.AlignType.Ins:
                                score.Insertions++;
                                break;
                            case Alignment.Types.AlignType.Del:
                                score.Deletions++;
                                score.Count++;
                                break;
                            case Alignment.Types.AlignType.Sub:
                                score.Substitutions++;
                                score.Count++;
                                break;
                        }
                        break;
                }
            }
            score.GetResults();
            block.Score = score;
        }

        public static AlignBlock WithEvaluation(this AlignBlock alignBlock)
        {
            foreach (var a in alignBlock.Alignment)
            {
                if (a.AlignmentCase != Alignment.AlignmentOneofCase.AlignedLabel)
                {
                    continue;
                }
                var item = a.AlignedLabel;

                if (item.Res == null)
                {
                    if (item.Ref.LabelCase == Event.Types.Label.LabelOneofCase.Item)
                        item.DoScoring = true;
                    if (item.Ref.LabelCase != Event.Types.Label.LabelOneofCase.Speaker)
                        item.AType = Alignment.Types.AlignType.Del;
                    continue;
                }
                if (item.Ref == null)
                {
                    if (item.Res.LabelCase == Event.Types.Label.LabelOneofCase.Item)
                        item.DoScoring = true;
                    if (item.Res.LabelCase != Event.Types.Label.LabelOneofCase.Speaker)
                        item.AType = Alignment.Types.AlignType.Ins;
                    continue;
                }

                if (item.Ref.LabelCase == Event.Types.Label.LabelOneofCase.Item)
                    item.DoScoring = true;

                //HIT or SUB
                if (item.Ref.LabelCase != Event.Types.Label.LabelOneofCase.Speaker)
                {
                    if (item.Ref.GetValue().ToLowerInvariant() == item.Res.GetValue().ToLowerInvariant())
                        item.AType = Alignment.Types.AlignType.Hit;
                    else
                        item.AType = Alignment.Types.AlignType.Sub;
                }
            }
            return alignBlock;
        }


        public static List<AlignBlock> WithEvaluation(this List<AlignBlock> alignBlocks)
        {

            foreach (var ab in alignBlocks)
            {
                ab.WithEvaluation();
            }
            return alignBlocks;
        }


        public static List<AlignBlock> ToDummyAlignment(this List<Event> resList)
        {
            var ret = new AlignBlock() { BType = AlignBlock.Types.AlignBlockType.None };
            foreach (var e in resList)
            {
                switch (e.BodyCase)
                {
                    case Event.BodyOneofCase.Label:
                        ret.Alignment.Add(new Alignment
                        {
                            AlignedLabel = new Alignment.Types.AlignedLabel
                            {
                                Res = e.Label,
                                AType = Alignment.Types.AlignType.Ins,
                                DoScoring = e.Label.LabelCase == Event.Types.Label.LabelOneofCase.Item
                            }
                        });
                        break;
                    case Event.BodyOneofCase.Timestamp:
                        if (e.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_)
                        {
                            ret.Alignment.Add(new Alignment
                            {
                                AlignedTimeStamp = new Alignment.Types.AlignedTimeStamp
                                {
                                    Timestamp = e.Timestamp
                                }
                            });
                        }
                        break;
                }
            }
            return new List<AlignBlock> { ret };
        }
       

        public static List<AlignBlock> ToAlignBlocks(this List<List<AlignedItem>> alignBlocks, List<Events> refBlockList, List<Event> resList)
        {
            var refList = new List<Event>();
            foreach(var rr in refBlockList)
            {
                refList.AddRange(rr.Events_);
            }

            var ret = new List<AlignBlock>();
            AlignBlock cblock = null;
            int resPos = 0; int refPos = 0; int blockCnt = 0;
            int lastNonReferenceBlock = 0;
            foreach (var ab in alignBlocks)
            {
                cblock = new AlignBlock();
                ret.Add(cblock);
                foreach (var a in ab)
                {
                    var item = new Alignment() { AlignedLabel = new Alignment.Types.AlignedLabel() };
                    if (a.Result != null)
                    {
                        for (; resPos < a.Result.Index; resPos++)
                        {
                            if (resList[resPos].BodyCase == Event.BodyOneofCase.Timestamp)
                            {
                                var i = new Alignment { AlignedTimeStamp = new Alignment.Types.AlignedTimeStamp { Timestamp = resList[resPos].Timestamp } };
                                cblock.Alignment.Add(i);
                            }
                            if (resList[resPos].BodyCase == Event.BodyOneofCase.Label)
                            {
                                var i = new Alignment() { AlignedLabel = new Alignment.Types.AlignedLabel() };
                                i.AlignedLabel.Res = resList[resPos].Label;
                                cblock.Alignment.Add(i);
                            }
                        }

                        item.AlignedLabel.Res = resList[a.Result.Index].Label;
                        resPos++;
                    }
                    cblock.Alignment.Add(item);
                    if (a.Reference != null)
                    {
                        for (; refPos < a.Reference.Index; refPos++)
                        {
                            if (refList[refPos].BodyCase == Event.BodyOneofCase.Label)
                            {
                                var i = new Alignment() { AlignedLabel = new Alignment.Types.AlignedLabel() };
                                i.AlignedLabel.Ref = refList[refPos].Label;
                                cblock.Alignment.Add(i);
                            }
                        }

                        item.AlignedLabel.Ref = refList[a.Reference.Index].Label;
                        refPos++;
                    }
                    


                }
                cblock.WithEvaluation();
                if(blockCnt >0 && (!cblock.IsReference() || !ret[blockCnt - 1].IsReference()))
                {

                    var pblock = ret[blockCnt - 1];
                    //get all deletions from prev left left
                    var ldel = pblock.Alignment.Reverse().TakeWhile(
                        x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedLabel &&
                        (x.AlignedLabel.AType == Alignment.Types.AlignType.Del ||
                        !x.AlignedLabel.DoScoring)
                        ).Reverse().ToList();
                    var lrest = pblock.Alignment.Take(pblock.Alignment.Count() - ldel.Count()).ToList();

                    
                    
                    if(lrest.Count()  >0 && lrest.Last().AlignmentCase!= Alignment.AlignmentOneofCase.AlignedTimeStamp)
                    {
                        var rdel = cblock.Alignment.TakeWhile(
                        x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedLabel &&
                        (x.AlignedLabel.AType == Alignment.Types.AlignType.Del ||
                        !x.AlignedLabel.DoScoring)
                        ).ToList();
                        var rrest = cblock.Alignment.Skip(rdel.Count()).ToList();
                        if(rrest.Count() >0 && rrest.First().AlignmentCase== Alignment.AlignmentOneofCase.AlignedTimeStamp)
                        {
                            pblock.Alignment.Clear();
                            pblock.Alignment.AddRange(lrest);
                            pblock.Alignment.Add(rrest.First());
                            pblock.Alignment.Add(ldel);

                            cblock.Alignment.Clear();
                            cblock.Alignment.AddRange(rdel);
                            cblock.Alignment.AddRange(rrest.Skip(1));
                        }
                    }
                    /*
                    var cdel = cblock.Alignment.TakeWhile(
                       x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedLabel &&
                       (x.AlignedLabel.AType == Alignment.Types.AlignType.Del ||
                       !x.AlignedLabel.DoScoring)
                       ).ToList();
                    var crest = cblock.Alignment.Skip(cdel.Count()).ToList();
                    */

                    /*
                    if (cblock.IsReference())
                    {
                        var ts = cblock.Alignment.Where(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp).ToList();
                        var rest = cblock.Alignment.Where(x => x.AlignmentCase != Alignment.AlignmentOneofCase.AlignedTimeStamp).ToList();
                        cblock.Alignment.Clear();
                        cblock.Alignment.AddRange(rest);
                        ret[blockCnt - 1].Alignment.AddRange(ts);
                    }
                    else
                    {

                        var f = cblock.Alignment.ElementAtOrDefault(0);
                        var l = ret[blockCnt - 1].Alignment.LastOrDefault();

                        if (f != null)
                        {

                            if (f.AlignmentCase != Alignment.AlignmentOneofCase.AlignedTimeStamp && f.AlignedLabel.AType!= Alignment.Types.AlignType.Del)
                            {
                                var last = ret[blockCnt - 1].Alignment.LastOrDefault(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedTimeStamp && x.AlignedTimeStamp.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_);
                                if (last != null)
                                {
                                    cblock.Alignment.Insert(0, last.Clone());
                                }
                            }
                        }
                    }
                    */
                }
                
                if (!cblock.IsReference())
                {
                    lastNonReferenceBlock = blockCnt;
                }
               
                blockCnt++;
            }
            cblock = ret[lastNonReferenceBlock];
            for (; resPos < resList.Count; resPos++)
            {
                if (resList[resPos].BodyCase == Event.BodyOneofCase.Timestamp)
                {
                    var i = new Alignment { AlignedTimeStamp = new Alignment.Types.AlignedTimeStamp { Timestamp = resList[resPos].Timestamp } };
                    cblock.Alignment.Add(i);
                }
                if (resList[resPos].BodyCase == Event.BodyOneofCase.Label)
                {
                    var i = new Alignment() { AlignedLabel = new Alignment.Types.AlignedLabel() };
                    i.AlignedLabel.Res = resList[resPos].Label;
                    cblock.Alignment.Add(i);
                }
            }
            resPos++;

            return ret.WithEvaluation();
        }

        private static bool IsReference(this AlignBlock block)
        {
            var labels = block.Alignment.Where(x => x.AlignmentCase == Alignment.AlignmentOneofCase.AlignedLabel);
            var refs = labels.Count(x => x.AlignedLabel.Res == null || 
            x.AlignedLabel.Res.LabelCase== Event.Types.Label.LabelOneofCase.Noise || 
            x.AlignedLabel.Res.LabelCase== Event.Types.Label.LabelOneofCase.Plus);
            return refs == labels.Count();
        }


        public static List<TextItem> ToTextList(this List<Events> input, string plusPrefix, int start = 0, int count = 0)
        {
            var ret = new List<TextItem>();
            int blockId = 0; int offset = 0;
            foreach (var i in input)
            {
                var textlist = i.Events_.ToList().ToTetxList(plusPrefix,offset ,start, count).Select(x => { x.Block = blockId;  return x; } ).ToList();
                ret.AddRange(textlist);
                offset += i.Events_.Count;
                blockId++;
            }

            return ret;

        }

        public static List<TextItem> ToTetxList(this List<Event> input, string plusPrefix, int offset=0, int start = 0, int count = 0)
        {
            if (count <= 0)
                count = input.Count;
           
            return Enumerable.Range(start, count).Where(i => 
             (input[i].BodyCase == Event.BodyOneofCase.Label) && 
             ((input[i].Label.LabelCase != Event.Types.Label.LabelOneofCase.Noise && input[i].Label.LabelCase != Event.Types.Label.LabelOneofCase.Speaker))
            ).Select(i => {

                switch (input[i].Label.LabelCase)
                {
                    case Event.Types.Label.LabelOneofCase.Item:
                        return new TextItem(input[i].Label.Item, i+ offset);
                    case Event.Types.Label.LabelOneofCase.Noise:
                        return new TextItem(input[i].Label.Noise, i+ offset);
                    case Event.Types.Label.LabelOneofCase.Plus:
                        return new TextItem("#!" + plusPrefix + i.ToString("000000000"), i+ offset);
                    default:
                        throw new Exception("Invalid item");
                }

            }
            ).ToList();
           
        }
    }
}
