﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ntx.api.pipe
{
    public class NullWriter<T> : IAsyncSink<T>
    {
        public Task WriteAsync(T item)
        {
            return Task.CompletedTask;
        }
        public Task CompleteAsync()
        {
            return Task.CompletedTask;
        }
    }
}
