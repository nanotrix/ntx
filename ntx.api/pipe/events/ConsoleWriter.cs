﻿using cz.ntx.proto.v2t.engine;
using System;
using System.Threading.Tasks;

namespace ntx.api.pipe.events
{
    
    public class ConsoleWriter : IAsyncSink<Events>
    {
        class Position
        {
            public int Left { get; set; }
            public int Top { get; set; }
        }
        string prevOutput = "";
        bool prevLookahead = false;
        private Position fixPos=null;

        public ConsoleWriter()
        {
        }
        public Task CompleteAsync()
        {
            Console.WriteLine();
            Console.WriteLine("====");
            
            
            return Task.CompletedTask;
        }

        public Task WriteAsync(Events item)
        {
            if (fixPos == null)
            {
                Console.WriteLine();
                Console.WriteLine("====");
            }
            string output = "";
            foreach (var e in item.Events_)
            {
                if (e.BodyCase != Event.BodyOneofCase.Label)
                    continue;
                switch (e.Label.LabelCase)
                {
                    case Event.Types.Label.LabelOneofCase.Item:
                        output += e.Label.Item;
                        break;
                    case Event.Types.Label.LabelOneofCase.Plus:
                        output += e.Label.Plus;
                        break;
                    case Event.Types.Label.LabelOneofCase.Noise:
                        output += e.Label.Noise;
                        break;
                }
            }

            fixPos = new Position { Left = Console.CursorLeft, Top = Console.CursorTop };
            if (prevLookahead)
            {
                var empty = new String(' ', prevOutput.Length);
                Console.Write(empty);
                Console.CursorTop = fixPos.Top; Console.CursorLeft = fixPos.Left;
            }
            Console.Write(output);
            if (item.Lookahead)
            {
                Console.CursorTop = fixPos.Top; Console.CursorLeft = fixPos.Left;
            }
            prevOutput = output;
            prevLookahead = item.Lookahead;
            return Task.CompletedTask;
        }
    }
}
