﻿using cz.ntx.proto.v2t.engine;
using System;
using Dasync.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ntx.api.pipe.events
{
    public static partial class Extensions
    {
        public static Dasync.Collections.AsyncEnumerable<Events> ToCompactBlock(this Dasync.Collections.AsyncEnumerable<Event> eventSource)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = eventSource.GetAsyncEnumerator();
                bool hasStart = false;
                bool hasEnd = false;
                var eventsBlock = new Events();
                while (await source.MoveNextAsync())
                {
                    var e = source.Current;

                    if(e.BodyCase== Event.BodyOneofCase.Label &&
                      e.Label.LabelCase!= Event.Types.Label.LabelOneofCase.Item &&
                      hasEnd) 
                    {

                        await yield.ReturnAsync(eventsBlock);
                        var last = eventsBlock.Events_.Last(x => x.BodyCase == Event.BodyOneofCase.Timestamp);
                        eventsBlock = new Events();
                        eventsBlock.Events_.Add(last);
                        eventsBlock.Events_.Add(e);
                        hasEnd = false;
                        continue;
                    }
                    if (!hasStart)
                    {
                        if (e.BodyCase == Event.BodyOneofCase.Timestamp && e.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_)
                        {
                            eventsBlock.Events_.Add(e);
                        }else
                        {
                            eventsBlock.Events_.Add(new Event { Timestamp = new Event.Types.Timestamp { Timestamp_ = 0 } });
                            eventsBlock.Events_.Add(e);
                        }
                        hasStart = true;
                        continue;
                    }
                    if (e.BodyCase == Event.BodyOneofCase.Timestamp && e.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_)
                    {
                        hasEnd = true;
                        eventsBlock.Events_.Add(e);
                        continue;
                    }
                    
                    eventsBlock.Events_.Add(e);
                }
                if(eventsBlock.Events_.Count >0)
                {
                    await yield.ReturnAsync(eventsBlock);
                }
                
            });
        }
    }
}
