﻿using cz.ntx.proto.v2t.engine;
using Dasync.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.ComponentModel;

namespace ntx.api.pipe.events
{
    class NTX4TimeStamp
    {

        [JsonProperty("type")]
        public string type { get; set; }
        [JsonProperty("val")]
        public UInt64 val { get; set; }
        public static NTX4TimeStamp FromEvent(Event e)
        {
            switch (e.Timestamp.ValueCase)
            {
                case Event.Types.Timestamp.ValueOneofCase.Timestamp_:
                    return new NTX4TimeStamp() { type = "ts", val = e.Timestamp.Timestamp_ };

            }
            throw new Exception($"Invalid event : {e}");

        }

        
    }
    class NTX4Label
    {

        [JsonProperty("type")]
        public string type { get; set; }

        [JsonProperty("val")]
        public string val { get; set; }
        public static NTX4Label FromEvent(Event e)
        {
            switch (e.Label.LabelCase)
            {
                case Event.Types.Label.LabelOneofCase.Item:
                    return new NTX4Label() { type = "item", val = e.Label.Item };
                   
                case Event.Types.Label.LabelOneofCase.Noise:
                    return new NTX4Label() { type = "noise", val = e.Label.Noise };
                case Event.Types.Label.LabelOneofCase.Plus:
                    return new NTX4Label() { type = "plus", val = e.Label.Plus };
            }
            throw new Exception($"Invalid event : {e}");
        }
    }
    class NTX4Event
    {
        public static NTX4Event FromEvent(Event e)
        {
            switch (e.BodyCase)
            {
                case Event.BodyOneofCase.Label:
                    return new NTX4Event() { label = NTX4Label.FromEvent(e), type="label" };
                case Event.BodyOneofCase.Timestamp:
                    return new NTX4Event() { timeStamp = NTX4TimeStamp.FromEvent(e), type = "timestamp" };

            }
            throw new Exception($"Invalid event : {e}");
        }
        [JsonProperty("timestamp")]
        public NTX4TimeStamp timeStamp { get; set; }
        [JsonProperty("label")]
        public NTX4Label label { get; set; }

        
        [JsonProperty("type"),]
        public string type { get; set; }

        [DefaultValue(false)]
        [JsonProperty("lookahead")]
        public bool lookahead { get; set; }
    }
    public static partial class Extensions
    {
        public static Dasync.Collections.AsyncEnumerable<string> ToNTX4Format(this Dasync.Collections.AsyncEnumerable<Events> eventsSource)
        {
            return new AsyncEnumerable<string>(async yield =>
            {
            var source = eventsSource.GetAsyncEnumerator();
            var serset = new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore };
                while (await source.MoveNextAsync())
                {
                    bool lookahead = source.Current.Lookahead;
                    var events = source.Current.Events_;
                    foreach(var e in events)
                    {
                        if(!(e.BodyCase == Event.BodyOneofCase.Label || 
                        (e.BodyCase == Event.BodyOneofCase.Timestamp && 
                        e.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_)
                            ))
                        {
                            continue;
                        }
                        var ev= NTX4Event.FromEvent(e);
                        ev.lookahead = lookahead;
                        await yield.ReturnAsync(JsonConvert.SerializeObject(ev,serset) + "\n");
                    }
                }
            }
            );
        }
    }
}
