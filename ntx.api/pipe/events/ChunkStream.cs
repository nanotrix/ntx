﻿using cz.ntx.proto.v2t.engine;
using System;
using Dasync.Collections;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;

namespace ntx.api.pipe.events
{
    public static partial class Extensions
    {
        public static Dasync.Collections.AsyncEnumerable<string> ToStringChunkStream(this Dasync.Collections.AsyncEnumerable<Events> eventsSource)
        {

            return new AsyncEnumerable<string>(async yield =>
            {
                var source = eventsSource.GetAsyncEnumerator();
                string prevOutput = "";
                bool prevLookahead = false;
                while (await source.MoveNextAsync())
                {
                    string output = "";
                    foreach (var e in source.Current.Events_)
                    {
                        if (e.BodyCase != Event.BodyOneofCase.Label)
                            continue;
                        switch (e.Label.LabelCase)
                        {
                            case Event.Types.Label.LabelOneofCase.Item:
                                output += e.Label.Item;
                                break;
                            case Event.Types.Label.LabelOneofCase.Plus:
                                output += e.Label.Plus;
                                break;
                            case Event.Types.Label.LabelOneofCase.Noise:
                                output += e.Label.Noise;
                                break;
                        }
                    }
                    if (prevLookahead)
                    {
                        var delete = new String('\b', prevOutput.Length);
                        await yield.ReturnAsync(delete);
                    }
                    prevOutput = output;
                    prevLookahead = source.Current.Lookahead;
                    if (output.Length > 0)
                    {
                        await yield.ReturnAsync(output);
                    }
                }
            });
        }

        public static Dasync.Collections.AsyncEnumerable<byte[]> ToByteChunkStream(this Dasync.Collections.AsyncEnumerable<Events> audioSource)
        {

            return new AsyncEnumerable<byte[]>(async yield =>
            {
                var source = audioSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    foreach (var e in source.Current.Events_)
                    {
                        if (e.BodyCase != Event.BodyOneofCase.Audio)
                            continue;

                        if(e.Audio.Body.Length>0)
                        {
                            await yield.ReturnAsync(e.Audio.Body.ToByteArray());
                        }
                        
                    }
                }
                
            });
        }

        public static Dasync.Collections.AsyncEnumerable<Event> ToEventStream(this Dasync.Collections.AsyncEnumerable<Events> eventsSource)
        {

            return new AsyncEnumerable<Event>(async yield =>
            {
                var source = eventsSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    foreach (var e in source.Current.Events_)
                    {
                        await yield.ReturnAsync(e);
                    }

                }
            });
        }

        public static Dasync.Collections.AsyncEnumerable<Events> ToEventsStream(this Dasync.Collections.AsyncEnumerable<Event> eventSource)
        {

            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = eventSource.GetAsyncEnumerator();
                var cEvents = new Events();
                while (await source.MoveNextAsync())
                {
                    var current = source.Current;

                    if (current==null)
                    {
                        await yield.ReturnAsync(cEvents);
                        continue;
                    }
                    cEvents.Events_.Add(current);

                }
                await yield.ReturnAsync(cEvents);
            });
        }

        public static Dasync.Collections.AsyncEnumerable<Event> ViaInterceptor(this Dasync.Collections.AsyncEnumerable<Event> eventsSource, Func<Event,Event> intercept)
        {

            
            return new AsyncEnumerable<Event>(async yield =>
            {
                var source = eventsSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    var item = source.Current;
                    await yield.ReturnAsync(intercept(item.Clone()));
                }
            });
        }
        
        public static Dasync.Collections.AsyncEnumerable<Events> ViaLabelMerge(this Dasync.Collections.AsyncEnumerable<Events> eventsSource)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = eventsSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    var item = source.Current;
                    var ret = item.Clone();
                    ret.Events_.Clear();
                    var merged = await PipeSource.FromEnumerator(item.Events_.GetEnumerator()).ViaLabelMerge().ToListAsync();
                    ret.Events_.AddRange(merged);

                    await yield.ReturnAsync(ret);

                }
            });
        }
        public static Dasync.Collections.AsyncEnumerable<Event> ViaLabelItemTransform(this Dasync.Collections.AsyncEnumerable<Event> eventsSource,string match, string replace)
        {
            if (match == null)
            {
                return eventsSource;
            }

            return new AsyncEnumerable<Event>(async yield =>
            {
                var source = eventsSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    var item = source.Current;

                    



                    if (item.BodyCase == Event.BodyOneofCase.Label && item.Label.LabelCase == Event.Types.Label.LabelOneofCase.Item)
                    {

                        var ret = Regex.Replace(item.Label.Item, match, replace);

                        if (ret.Length == 0)
                            continue;
                        await yield.ReturnAsync(new Event
                        {
                            Label = new Event.Types.Label{ Item = ret}
                        }
                        );

                    }
                    else
                    {
                        await yield.ReturnAsync(item);
                    }

                }
            });
        }
        public static Dasync.Collections.AsyncEnumerable<Event> ViaLabelMerge(this Dasync.Collections.AsyncEnumerable<Event> eventsSource)
        {

            return new AsyncEnumerable<Event>(async yield =>
            {
                var source = eventsSource.GetAsyncEnumerator();
                List<Event> block = new List<Event>();
                Event lastItem = null;
                while (await source.MoveNextAsync())
                {
                    var item =source.Current; 

                    if(item.BodyCase!= Event.BodyOneofCase.Label )
                    {
                        block.Add(item);
                        continue;
                    }
                    if (lastItem == null)
                    {
                        foreach(var i in block)
                        {
                            await yield.ReturnAsync(i);
                        }
                        block.Clear();
                        lastItem = item;
                        continue;
                    }
                    if (lastItem.Label.LabelCase == item.Label.LabelCase)
                    {
                        bool canContinue=true;
                        switch (lastItem.Label.LabelCase)
                        {
                            case Event.Types.Label.LabelOneofCase.Item:
                                block.Clear();
                                lastItem.Label.Item = lastItem.Label.Item + item.Label.Item;
                                break;
                            case Event.Types.Label.LabelOneofCase.Plus:
                                block.Clear();
                                lastItem.Label.Plus = lastItem.Label.Plus + item.Label.Plus;
                                break;
                            case Event.Types.Label.LabelOneofCase.Noise:
                                if(lastItem.Label.Noise==item.Label.Noise)
                                {
                                    block.Clear();
                                }
                                else
                                {
                                    canContinue = false;
                                }
                                break;
                        }

                        if(canContinue)
                            continue;   
                    }

                    await yield.ReturnAsync(lastItem);
                    foreach (var i in block)
                    {
                        await yield.ReturnAsync(i);
                    }
                    block.Clear();
                    lastItem = item;
                }
                if (lastItem != null)
                    await yield.ReturnAsync(lastItem);
                foreach (var i in block)
                {
                    await yield.ReturnAsync(i);
                }
            });
        }

    }
}

