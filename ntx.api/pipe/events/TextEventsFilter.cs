﻿using cz.ntx.proto.v2t.engine;
using cz.ntx.proto.v2t.misc;
using System;
using Dasync.Collections;
using System.Linq;
using System.Text.RegularExpressions;

namespace ntx.api.pipe.events
{
    public static partial class Extensions
    {
        public static AsyncEnumerable<Events> ViaFilterAsync(this AsyncEnumerable<Events> source, EventFilter filter)
        {
            switch (filter.FilterCase)
            {
                case EventFilter.FilterOneofCase.Noise:
                    return source.StringSourceNoiseFilter(filter.Noise);
                case EventFilter.FilterOneofCase.None:
                    return source.NoneFilter();
                default:
                    throw new Exception("Invalid filter type: " + filter.FilterCase.ToString());

            }
        }

        public static AsyncEnumerable<Events> ViaLookAheadFilter(this AsyncEnumerable<Events> eventSource)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = eventSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    if (source.Current == null)
                    {
                        await yield.ReturnAsync(null);
                        continue;
                    }
                    if(!source.Current.Lookahead)
                    {
                        await yield.ReturnAsync(source.Current.Clone());
                        continue;
                    }

                }
            }
            );
        }

        public static AsyncEnumerable<Events> ViaEventFilter(this AsyncEnumerable<Events> eventSource, Func<Event,Event> filter )
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = eventSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    if (source.Current == null)
                    {
                        await yield.ReturnAsync(null);
                        continue;
                    }

                    var ret = source.Current.Clone();
                    ret.Events_.Clear();
                    foreach (var e in source.Current.Events_)
                    {
                        var r = filter(e);
                        if (r != null)
                            ret.Events_.Add(r);
                    }

                    await yield.ReturnAsync(ret);
                    continue;
                    

                }
            }
            );
        }

        public static AsyncEnumerable<Events> ViaEventsLimiterByCount(this AsyncEnumerable<Events> eventSource, int maxEvents)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = eventSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    if (source.Current == null)
                    {
                        await yield.ReturnAsync(null);
                        continue;
                    }
                    var ev = source.Current;
                    var rest= ((ev.Events_.Count) % maxEvents) > 0 ? 1 : 0; ;
                    int noBatches = (ev.Events_.Count) / maxEvents + rest;
                    var src = ev.Events_.Clone();
                    ev.Events_.Clear();
                    int skip = 0;
                    for(int i=0;i<noBatches;i++)
                    {
                        var ret= ev.Clone();
                        ret.Events_.AddRange(src.Skip(skip).Take(maxEvents));
                        skip += maxEvents;
                        await yield.ReturnAsync(ret);

                    }

                }
            }
            );
        }

        public static AsyncEnumerable<Events> ViaInterceptor(this AsyncEnumerable<Events> eventSource, Func<Events,Events> intercept, bool removeNull=false)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = eventSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    if (source.Current == null)
                    {
                        await yield.ReturnAsync(null);
                        continue;
                    }

                    var ret = intercept(source.Current.Clone());
                    if (ret == null && removeNull)
                        continue;

                    await yield.ReturnAsync(ret);
                    continue;
                    

                }
            }
            );
        }

        public static AsyncEnumerable<Events> ViaReLabelAndSplit(this AsyncEnumerable<Events> eventSource, Regex split, Regex plus)
        {
            Events interceptor(Events x)
            {
                var ret = x.Clone();
                ret.Events_.Clear();
                foreach (var v in x.Events_)
                {
                    if (v.BodyCase != Event.BodyOneofCase.Label)
                    {
                        ret.Events_.Add(v);
                        continue;
                    }
                    if (v.Label.LabelCase != Event.Types.Label.LabelOneofCase.Item &&
                    v.Label.LabelCase != Event.Types.Label.LabelOneofCase.Plus
                    )
                    {
                        ret.Events_.Add(v);
                        continue;
                    }

                    var line = v.Label.GetValue();
                    foreach (var s in split.Split(line))
                    {
                        if (s == "")
                            continue;
                        if (plus.IsMatch(s))
                        {
                            ret.Events_.Add(
                                new Event() { Label = new Event.Types.Label { Plus = s } }
                                );
                        }
                        else
                        {
                            ret.Events_.Add(
                                new Event() { Label = new Event.Types.Label { Item = s } }
                                );
                        }

                    }




                }
                if (ret.Events_.Count == 0)
                    return null;

                return ret;
            }

            return eventSource.ViaInterceptor(interceptor,true);
        }

        private static AsyncEnumerable<Events> NoneFilter(this AsyncEnumerable<Events> eventSource)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = eventSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    var ret = source.Current.Clone();
                    await yield.ReturnAsync(ret);
                    
                }

            });
        }
        
        private static AsyncEnumerable<Events> StringSourceNoiseFilter(this AsyncEnumerable<Events> eventSource, EventFilter.Types.NoiseEventFilter filter)
        {
            return new AsyncEnumerable<Events>(async yield => {
                var source = eventSource.GetAsyncEnumerator();
                long speechHead = 0; long noiseHead = 0; bool canPrint = false;
                Event.Types.Label.LabelOneofCase lastSeen = Event.Types.Label.LabelOneofCase.None;
                while (await source.MoveNextAsync())
                {
                    var ret = new Events() { Lookahead = source.Current.Lookahead };
                    foreach (var e in source.Current.Events_)
                    {
                        switch (e.BodyCase)
                        {
                            case Event.BodyOneofCase.Timestamp:
                                switch (lastSeen)
                                {
                                    case Event.Types.Label.LabelOneofCase.Noise:
                                        if ((long)e.Timestamp.Recovery > noiseHead)
                                            noiseHead = (long)e.Timestamp.Recovery;
                                        break;
                                    case Event.Types.Label.LabelOneofCase.Item:
                                    case Event.Types.Label.LabelOneofCase.Plus:
                                        if ((long)e.Timestamp.Recovery > speechHead)
                                            speechHead = (long)e.Timestamp.Recovery;
                                        break;
                                }
                                break;
                            case Event.BodyOneofCase.Label:
                                lastSeen = e.Label.LabelCase;
                                switch (e.Label.LabelCase)
                                {
                                    case Event.Types.Label.LabelOneofCase.Noise:
                                        if (canPrint && ((noiseHead - speechHead) > (long)filter.Duration))
                                        {
                                            canPrint = false;
                                            ret.Events_.Add(new Event() { Label = new Event.Types.Label { Noise = filter.ParagraphSymbol } });
                                        }
                                        break;
                                    case Event.Types.Label.LabelOneofCase.Item:
                                    case Event.Types.Label.LabelOneofCase.Plus:
                                        canPrint = true;
                                        ret.Events_.Add(e);
                                        break;
                                }
                                break;
                        }
                    }
                    if (ret.Events_.Count > 0)
                        await yield.ReturnAsync(ret);
                }
            });
        }
    }
}
