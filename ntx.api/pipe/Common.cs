﻿using System;
using Dasync.Collections;
using System.Threading.Tasks;

namespace ntx.api.pipe
{
    public static partial class Pipe
    {
        public static async Task RunWithSink<T>(this AsyncEnumerable<T> source, IAsyncSink<T> sink)
        {
            await source.ForEachAsync(async (x) => await sink.WriteAsync(x));
            await sink.CompleteAsync();
        }

       
        public static async Task<T> ViaInterceptor<T>(this Task<T> taskSource, Func<T,T> interceptor)
        {
            return interceptor(await taskSource);
        }
    }
}
