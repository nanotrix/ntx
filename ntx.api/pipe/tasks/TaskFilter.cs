﻿using System;
using Dasync.Collections;
using System.Collections.Generic;
using System.Linq;

using Async = Dasync.Collections;

namespace ntx.api.pipe.tasks
{
    public static partial class Extensions
    {
        public static Async.AsyncEnumerable<cz.ntx.proto.license.Task> ViaFilterAsync(this Async.AsyncEnumerable<cz.ntx.proto.license.Task> taskSource, Func<cz.ntx.proto.license.Task, cz.ntx.proto.license.Task> select)
        {
            return new Async.AsyncEnumerable<cz.ntx.proto.license.Task>(async yield =>
            {
                var source = taskSource.GetAsyncEnumerator();
                while (await source.MoveNextAsync())
                {
                    var ret = select(source.Current);
                    if (ret!=null)
                    {
                        await yield.ReturnAsync(ret);
                    }
                }
            }
            );
        }

        public static Async.AsyncEnumerable<cz.ntx.proto.license.Task> ViaFilterAsync(this Async.AsyncEnumerable<cz.ntx.proto.license.Task> taskSource, string serviceLabel,string taskLabel,string[] tags)
        {
            Func<cz.ntx.proto.license.Task, cz.ntx.proto.license.Task> serviceFunc;
            if (serviceLabel == null)
            {
                serviceFunc = x => x;
            }
            else
            {
                serviceFunc = (x) =>
                {
                    if(x.Service.Contains(serviceLabel))
                    {
                        return x;
                    }
                    else
                    {
                        return null;
                    }
                };
            }



            Func<cz.ntx.proto.license.Task, cz.ntx.proto.license.Task> taskFunc;
            if (taskLabel == null)
            { 
                taskFunc = x => x;
            }
            else
            {
                taskFunc = (x) => {
                    var ret = x.Clone();
                    ret.Profiles.Clear();
                    foreach (var p in x.Profiles)
                    {
                        if (p.Labels.Contains(taskLabel))
                        {
                            var pp = p.Clone();
                            ret.Profiles.Add(pp);
                            pp.Labels.Clear();
                            pp.Labels.Add(taskLabel);
                        }

                    }
                    if(ret.Profiles.Count == 0)
                    {
                        return null;
                    }
                    return ret;
                };
            }
            Func<cz.ntx.proto.license.Task, cz.ntx.proto.license.Task> tagsFunc;
            
            if (tags == null)
            {
                tagsFunc = x => x;
            }
            else
            {
                //make it clear
                tagsFunc = (x) => {
                    foreach (var s in tags){
                        if (!x.Tags.Contains(s))
                            return null;
                    }
                    return x;
                };
            }
            return taskSource.ViaFilterAsync(serviceFunc).ViaFilterAsync(taskFunc).ViaFilterAsync(tagsFunc);
        }

        



    }
}
