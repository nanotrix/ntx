﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ntx.api.auth
{
    public class JWT
    {
        public static string FromBase64String(string input)
        {
            input = input.TrimEnd('=').Replace('_', '/').Replace('-', '+');
            if ((input.Length * 3 )% 4 > 0)
                input += "=";
            if ((input.Length * 3) % 4 > 0)
                input += "=";
            return Encoding.UTF8.GetString(Convert.FromBase64String(input));
        }
        public static string ToBase64String(string input)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(input), Base64FormattingOptions.None).Replace('+','-').Replace('/','_').TrimEnd('=');
        }
        public static T Payload<T>(string token)
        {
            var parts = token.Split('.');
            if (parts.Length!=3)
            {
                throw new Exception("Invalid JWT token");
            }
            return JsonConvert.DeserializeObject<T>(FromBase64String(parts[1]));
        }
    }
}
