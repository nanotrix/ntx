using Xunit;
using Xunit.Abstractions;
using cz.ntx.proto.v2t.engine;
using ntx.api;
using ntx.api.util;

namespace test
{
    public class CmdParseTest
    {
        
        public CmdParseTest()
        {
            
        }
        [Theory]
        [InlineData("auto:0")]
        [InlineData("pcm:s16le:16000:mono")]
        [InlineData("pcm:s16le:16000:stereo")]
        [InlineData("ismp")]
        public void AudioFormatShouldParse(string uri)
        {

            var ex = Record.Exception(
                () =>
                {
                    var audioFormat = new AudioFormat();
                    audioFormat.ParseCmd(uri);
                }
                );
            Assert.Null(ex);
        }
        [Theory]
        [InlineData("auto:-1")]
        [InlineData("auto")]
        [InlineData("nonsense")]
        [InlineData("pcm:s16le:16010:mono")]
        [InlineData("pcm:s16le:16000:quatro")]
        [InlineData("ismp:123")]
        public void AudioFormatShouldThrow(string cmd)
        {

            var ex = Record.Exception(
                () =>
                {
                    var audioFormat = new AudioFormat();
                    audioFormat.ParseCmd(cmd);
                }
                );
            Assert.NotNull(ex);
        }

        [Theory]
        [InlineData("vad", EngineContext.ConfigOneofCase.Vad,false,false,false)]
        [InlineData("ppc", EngineContext.ConfigOneofCase.Ppc,false,false,false)]
        [InlineData("v2t", EngineContext.ConfigOneofCase.V2T,false,false,false)]
        [InlineData("v2t+vad", EngineContext.ConfigOneofCase.V2T,true,false,false)]
        [InlineData("v2t+ppc", EngineContext.ConfigOneofCase.V2T, false, true,false)]
        [InlineData("ppc+v2t+vad", EngineContext.ConfigOneofCase.V2T, true,true,false)]
        [InlineData("ppc+v2t+vad+pnc", EngineContext.ConfigOneofCase.V2T, true, true, true)]
        public void EngineContextShouldParseAs(string cmd, EngineContext.ConfigOneofCase config, bool withVAD, bool withPPC,bool withPNC)
        {
             var ctx = new EngineContext();
             ctx.ParseCmd(cmd,"downmix");
            Assert.True(ctx.ConfigCase == config);
            if(config== EngineContext.ConfigOneofCase.V2T)
            {
               Assert.Equal(ctx.V2T.WithVAD != null,withVAD);
               Assert.Equal(ctx.V2T.WithPPC != null, withPPC);
                Assert.Equal(ctx.V2T.WithPNC != null, withPNC);
            }
        }

        [Theory]
        [InlineData("nonsense")]
        [InlineData("vad+ppc")]
        [InlineData("vad113")]
        public void EngineContextShouldFail(string cmd)
        {
            var ex = Record.Exception(
               () =>
               {
                   var ctx = new EngineContext();
                   ctx.ParseCmd(cmd,"downmix");
               }
               );
            Assert.NotNull(ex);
        }
    }
}
