﻿using ntx.api.auth;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace test
{
    public class HelperTest
    {
        [Theory]
        [InlineData("a")]
        [InlineData("ab")]
        [InlineData("abc")]
        [InlineData("abcd")]
        public void Base64Padding(string test)
        {

            Assert.Equal(test, JWT.FromBase64String(JWT.ToBase64String(test)));

        }
        [Theory]
        [InlineData("Cauz4vKG3IRt71AwEZxbMIsjNz9LR1IOcfIhGlLESEflBg6fI0HJSDBM70rh7C_iLx5Ps237brIsocX5Zp6dzx2cf3sigddILC1xCk7F_93y8NSxEdI2jwlfo7ZrCgWsjuSR4ij9IWQxUT-g2X4B_KGep-6dkmLvS6unw-im8D2B88RyJW97K15Hcn94uqYNPhoX87eX6bi6P2AWalTXbNe-0K3haazRYbEWaPuiBTPb5rAXwqyDlvwpQZlULMIZsmiVqiJxtFhbiH4NqpyqyW96lKxZNikYg35GCjBDm5rPMH4z9gdsw8eOA-08T0sBLoeBn8_2mW6NMC8EYtnZgg")]
        public void Base64Binary(string item)
        {

            var ex = Record.Exception(
                () =>
                {
                    var ret = JWT.FromBase64String(item);

                });
            Assert.Null(ex);
        }
    }
}
