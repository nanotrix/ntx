﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.Threading.Tasks;
using ntx.api.pipe.events;
using Microsoft.Extensions.Logging;
using ntx.api;

namespace ntx.command.devel.conv.trsx
{
    class Command : ICommand
    {
       
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.command.devel.conv.trsx");
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "converts events to trsx format";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes: ntx.v2t.engine.Events from diar engine"
             + Environment.NewLine
             + "Produces: trsx format"
             + Environment.NewLine + Environment.NewLine;

             var inputUriOption = command.Option("-i|--input",
                "input url of diarization events file",
                CommandOptionType.SingleValue
                );
            var transUriOption = command.Option("-t|--transcription <none>",
                "input url of diar events file",
                CommandOptionType.SingleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );
            var mediaUriOption = command.Option("-m|--media <none>",
                "media uri",
                CommandOptionType.SingleValue
                );

            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    TransUriOption = transUriOption.GetValueOrDefault(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    MediaUriOption = mediaUriOption.GetValueOrDefault()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string TransUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private string MediaUriOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var outputResolver = LazyStream.Output(OutputUriOption, "text/plain", breaker);
            var textSink = PipeSink.StringChunkStream(outputResolver);
          
            using (var inputResolver = LazyStream.Input(InputUriOption, breaker))
            {
                if (TransUriOption == "none")
                {
                    await PipeSource.FromProtoJsonStream<Events>(inputResolver, breaker)
                    .ViaLookAheadFilter()
                    .ToEventStream()
                    .ToTRSXFormat(MediaUriOption)
                    .RunWithSink(textSink);
                }
                else
                {
                    using (var transResolver = LazyStream.Input(TransUriOption, breaker))
                    {
                        await PipeSource.FromProtoJsonStream<Events>(inputResolver, breaker)
                       .ViaLookAheadFilter()
                       .ToEventStream()
                       .ToTRSXFormat(MediaUriOption,
                        PipeSource.FromProtoJsonStream<Events>(transResolver, breaker)
                       .ViaLookAheadFilter()
                       .ToEventStream()
                       )
                       .RunWithSink(textSink);
                    }
                }
            }
            

            return 0;
        }
    }
}