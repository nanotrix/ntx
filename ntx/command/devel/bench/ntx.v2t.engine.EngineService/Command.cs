﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using Dasync.Collections;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using ntx.api.auth;
using ntx.api.pipe.events;
using ntx.api;
using ntx.api.pipe.tasks;
using Microsoft.Extensions.Logging;
using ntx.api.util;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ntx.command.devel.bench.ntx.v2t.engine.EngineService
{
    class Command : ICommand
    {
        const string serviceId = "ntx.v2t.engine.EngineService";
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger(serviceId);
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "streaming media transcription client";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText = 
                "Consumes: binary audio stream" 
                + Environment.NewLine 
                + "Produces: ntx.v2t.engine.Events";
            var inputUriOption = command.Option("-i|--input", 
                "input url", 
                CommandOptionType.SingleValue
                );
            
            var outputUriOption = command.Option("-o|--output <->",
                "output url", 
                CommandOptionType.SingleValue
                );
            var labelOption = command.Option("-l|--label",
                "task label => see: ntx task list",
                CommandOptionType.SingleValue
                );
            var tagsOption = command.Option(@"-t|--tags",
              "task tags => see:  ntx task list",
              CommandOptionType.MultipleValue
              );
            var customTaskOption = command.Option(@"--task",
              "use custom task",
              CommandOptionType.SingleValue
              );
            var audioFormatOption = command.Option("-a|--audioformat <auto:0>",
                "format auto:$probesizeBytes|pcm:$sampleFormat:$sampleRate:$channelLayout",
                CommandOptionType.SingleValue
                );
            var chunkSizeOption = command.Option($"-c|--chunkSize <4096>",
               "input chunk size in bytes",
               CommandOptionType.SingleValue
               );
            var lexiconOption = command.Option($"--lexicon <none>",
              "user lexicon uri",
              CommandOptionType.SingleValue
              );
            var parallelism = command.Option("-p|--parallelism <1:1:1>"
              , "use parallelism"
             , CommandOptionType.SingleValue);

            var retryOption = command.Option("--retry <5:1000:1.5>"
               , "retry with exponencial backof count:initDelayMs:multiplier"
              , CommandOptionType.SingleValue);
            var channelOption = command.Option("--channel <downmix>"
               , "choose audio channel, dowmix|left|right"
              , CommandOptionType.SingleValue);
            var pushPullModel = command.Option("--fc"
               , "use push-pull model"
              , CommandOptionType.NoValue);

            command.OnExecute(() =>
            {
                if(!inputUriOption.HasValue())
                {
                    command.Error.WriteLine($"Missing: {inputUriOption.Description}");
                    command.ShowHelp();
                    return 1;
                }


                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value() ,
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    LabelOption = labelOption.GetValueOrDefault(),
                    TagsOption = tagsOption.HasValue() ? tagsOption.Values.ToArray() : null,
                    AudioFormatOption = audioFormatOption.GetValueOrDefault(),
                    ChunkSizeBytes = uint.Parse(chunkSizeOption.GetValueOrDefault()),
                    GlobalOptions = options,
                    LexiconUriOption = lexiconOption.GetValueOrDefault(),
                    Paralelism = parallelism.GetValueOrDefault(),
                    Retry = retryOption.GetValueOrDefault(),
                    ChannelOption = channelOption.GetValueOrDefault(),
                    PushPullModelOption = pushPullModel.HasValue(),
                    CustomTaskUri = customTaskOption.HasValue() ? customTaskOption.Value() : null,
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private string LabelOption { get; set; }
        private string AudioFormatOption { get; set; }
        private string[] TagsOption { get; set; }
        private string CustomTaskUri { get; set; }
        private CommandLineOptions GlobalOptions { get; set; }
        private string LexiconUriOption { get; set; }
        private string Paralelism { get; set; }
        private bool NoMerge { get; set; }
        private TimeSpan Overlap { get; set; }
        private string Retry { get; set; }
        private string ChannelOption { get; set; }
        private uint ChunkSizeBytes { get; set; }
        private bool PushPullModelOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async System.Threading.Tasks.Task<int> RunAsync(CancellationToken breaker)
        {
            var retry = RetryWithBackoff.ParseFromCmd(Retry);


            _logger.LogInformation($"Fetching input stream: {InputUriOption}");
            byte[] streamData = null;

            using (var mem = new MemoryStream())
            {
                await LazyStream.Input(InputUriOption, breaker).CopyToAsync(mem);
                streamData = mem.ToArray();
            }


            //stream with valid token
            var tokenStream = LazyStream.Input(GlobalOptions.TaskAuthToken);
            //valid access token generator
            var accessTokenFactory = Authorization.AccesTokenFactory(tokenStream);
            //select task and authorize it 

            //Use task selector to choose one
            var task= await PipeSource.TasksFromLocalStore(accessTokenFactory, breaker)
                .ViaFilterAsync(serviceId, LabelOption, TagsOption).FirstAsync().ViaInterceptor(
                    x =>
                    {
                        _logger.LogInformation($"Using task: {x.Service} -l {x.Profiles[0].Labels[0]} -t {string.Join(" -t ", x.Tags)}");
                        return x;
                    }
                );

            var authorizedTaskFactory = CustomTaskUri == null ? task.ToAuthorizedTaskFactory(accessTokenFactory) : (await new cz.ntx.proto.scheduler.TaskConfiguration().
                ParseFromStream(LazyStream.Input(CustomTaskUri))).
                ToAuthorizedTaskFactory(accessTokenFactory, LabelOption);



            var outputResolver = LazyStream.Output(OutputUriOption, "application/json",breaker);
            var _writer = new StreamWriter(outputResolver, new UTF8Encoding(false));
            await _writer.WriteLineAsync($"Parallelism;AudioDuration[s];ProcessingTime[s];RealTime[s];Speed;Throughput");
            //sevice Uri
            var endpointUri = new Uri((await accessTokenFactory()).ServiceEndpoint);
            //Create grpc channel, channel can be used for multiple clients of various services
            var channel = endpointUri.ToGRPCChannel();

            //Fill v2t config
            var engineContext = new EngineContext()
                .ParseCmd(LabelOption,ChannelOption);
            engineContext.AudioFormat = new AudioFormat()
                .ParseCmd(AudioFormatOption); ;

            //Add lexicon if any
            await engineContext.AddLexiconFromUri(LexiconUriOption);

            //Select reader
            bool textInput = engineContext.ConfigCase == EngineContext.ConfigOneofCase.Ppc;


            async Task<Tuple<long, long>> process()
            {

                var inputStreamResolver = new MemoryStream(streamData);

                //Create pipe source
                var eventsSource = textInput ?
                    PipeSource.FromProtoJsonStream<Events>(inputStreamResolver, breaker)
                    .ViaEventsLimiterByCount(10)
                    : PipeSource.EventsFromBinaryStream(inputStreamResolver, (int)ChunkSizeBytes, breaker);

                //Create client on channel
                var client = new cz.ntx.proto.v2t.engine.EngineService.EngineServiceClient(channel);


                //Create sink from output stream
                var sink = PipeSink.ProtoJsonStream<Events>(
                     outputResolver
                    );

                ulong maxProcessing = 0;
                ulong maxDuration = 0;

                //Run pipe
                await eventsSource.ViaV2TStreaming(client, engineContext, authorizedTaskFactory,breaker, PushPullModelOption)
                .ViaInterceptor((x) =>
               {
                   if (x.ReceivedAt > maxProcessing)
                       maxProcessing = x.ReceivedAt;
                   var ts = x.Events_.ToList().LastOrDefault(e => e.BodyCase == Event.BodyOneofCase.Timestamp);
                   if (ts != null)
                       if (ts.Timestamp.GetValue() > maxDuration)
                           maxDuration = ts.Timestamp.GetValue();
                   return x;
               })
                .RunWithSink(PipeSink.NullSink<Events>());
                return new Tuple<long, long>((long)maxProcessing, (long)maxDuration);
            }

            var range = Paralelism.Split(':');
            if (range.Length != 3)
                throw new Exception($"Invalid paralelism range: {Paralelism}");


            for(int parallelism = int.Parse(range[0]); parallelism <= int.Parse(range[2]);parallelism+= int.Parse(range[1]))
            {
                var _tasklist = new List<Task<Tuple<long, long>>>();
                var start = DateTime.UtcNow;
                for (int i = 0; i < parallelism; i++)
                {
                    _tasklist.Add(process());
                }
                await Task.WhenAll(_tasklist);

                long totalProcessing = 0; long totalDuration = 0;
                foreach (var t in _tasklist)
                {
                    var ret = await t;
                    totalProcessing += ret.Item1;
                    totalDuration += ret.Item2;
                }
                var rtDur = (DateTime.UtcNow - start);
                var n = _tasklist.Count;
                await _writer.WriteLineAsync($"{n};{TimeSpan.FromTicks(totalDuration).TotalSeconds};{TimeSpan.FromTicks(totalProcessing).TotalSeconds};{rtDur.TotalSeconds};{(double)totalDuration / (double)totalProcessing};{(double)totalDuration / (double)(rtDur.Ticks)}");
                await _writer.FlushAsync();
            }
            return 0;
        }


    }
}
