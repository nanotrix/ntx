﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.IO;
using ntx.api.pipe.tasks;
using System.Threading.Tasks;
using cz.ntx.proto.v2t.misc;
using Dasync.Collections;
using ntx.api.pipe.events;
using System.Text;
using ntx.api.util;
using ConsoleTables;
using Microsoft.Extensions.Logging;
using ntx.api;
using System.Collections.Generic;

namespace ntx.command.util.events.eval.summary
{
    class Command : ICommand
    {
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.command.util.events.eval.summary");
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "create summary over eval results";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes:  ntx.v2t.misc.Evaluation.Item"
             + Environment.NewLine
             + "Produces: ntx.v2t.misc.Evaluation"
             + Environment.NewLine + Environment.NewLine;

             var inputUriOption = command.Option("-i|--input",
                "multiple input urls",
                CommandOptionType.MultipleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
             "output url",
             CommandOptionType.SingleValue
             );

            var accuracyOption = command.Option("-a|--minaccuracy <0.0>",
             "test min accuracy",
             CommandOptionType.SingleValue
             );

            var speedOption = command.Option("-s|--minspeed <0.0>",
             "test min speed factor",
             CommandOptionType.SingleValue
             );
            var outFormat = command.Option("-g|--oformat <txt>",
               "output format txt|json|diar",
               CommandOptionType.SingleValue
               );
            var withDetails = command.Option("--withDetails",
                "use detailed output",
               CommandOptionType.NoValue
               );
            var isFileListInput = command.Option("--filelist",
               "use filelist as input",
              CommandOptionType.NoValue
              );
            var filesuffix = command.Option("--filesuffix <>",
               "append suffix to filelist's items",
               CommandOptionType.SingleValue
               );
            var fileprefix = command.Option("--fileprefix <>",
               "append prefix to filelist's items",
               CommandOptionType.SingleValue
               );

            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Values.ToArray(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    MinAccuracy = float.Parse(accuracyOption.GetValueOrDefault()),
                    MinSpeed = float.Parse(speedOption.GetValueOrDefault()),
                    OutFormat = outFormat.GetValueOrDefault(),
                    WithDetails = withDetails.HasValue(),
                    InputAsFileList = isFileListInput.HasValue(),
                    FileSuffix = filesuffix.GetValueOrDefault(),
                    FilePrefix = fileprefix.GetValueOrDefault()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string[] InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private float MinAccuracy { get; set; }
        private float MinSpeed { get; set; }
        private string OutFormat { get; set; }
        private string FileSuffix { get; set; }
        private string FilePrefix { get; set; }
        private bool WithDetails { get; set; }
        private bool InputAsFileList { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {

            var result = new Evaluation();

            var fileList = new List<string>();
            if(InputAsFileList)
            {
                foreach (var item in InputUriOption)
                {
                    using (var inputResolver = LazyStream.Input(item, breaker))
                    {
                        using (StreamReader _stream = new StreamReader(inputResolver, new UTF8Encoding()))
                        {
                            string line;
                            while((line = (await _stream.ReadLineAsync()))!=null)
                            {
                                line = line.Trim();
                                if (line.Length == 0)
                                    continue;
                                fileList.Add(FilePrefix + line + FileSuffix);
                            }
                        }
                    }
                }

            }else
            {
                fileList.AddRange(InputUriOption);
            }
            foreach (var item in fileList)
            {


                using (var inputResolver = LazyStream.Input(item, breaker))
                {
                    using (StreamReader _stream = new StreamReader(inputResolver, new UTF8Encoding()))
                    {
                        var line = await _stream.ReadToEndAsync();
                        var content = Evaluation.Types.Item.Parser.ParseJson(line);
                        result.Items.Add(content);
                    }


                }
            }

            result.GetResults();
            if (!WithDetails)
            {
                foreach(var i in result.Items)
                {
                    i.Blocks.Clear();
                }
            }


            switch (OutFormat)
            {
                case "txt":
                    using (var output = new StreamWriter(LazyStream.Output(OutputUriOption, "text/plain"), new UTF8Encoding(false)))
                    {
                        var table = new ConsoleTable("Id", $"Acc[%]({100 * MinAccuracy})", "Corr[%]", $"Speed[dur/proc]({MinSpeed})");
                        if(WithDetails)
                            table = new ConsoleTable("Id",$"Acc[%]({100*MinAccuracy})", "Corr[%]", $"Speed[dur/proc]({MinSpeed})", "Dur","Proc","N","H","S","I","D");
                        int no = 0;
                        foreach (var v in result.Items)
                        {
                            var acc = v.Score == null ? "-" : String.Format("{0:0.00}",100 * v.Score.Accuracy);
                            var corr = v.Score == null ? "-" : String.Format("{0:0.00}", 100 * v.Score.Correctness);

                            var speed= v.Speed==null ? "-" : String.Format("{0:0.00}", v.Speed.Factor);
                            var dur = v.Speed == null ? "-" : TimeSpan.FromTicks((long)v.Speed.StreamDuration).ToString();
                            var pt = v.Speed == null ? "-" : TimeSpan.FromTicks((long)v.Speed.ProcessingDuration).ToString();

                            var num = v.Score == null ? "-" : v.Score.Count.ToString();
                            var hits = v.Score == null ? "-" : v.Score.Hits.ToString();
                            var subs = v.Score == null ? "-" : v.Score.Substitutions.ToString();
                            var ins = v.Score == null ? "-" : v.Score.Insertions.ToString();
                            var del = v.Score == null ? "-" : v.Score.Deletions.ToString();

                            if (WithDetails)
                                table.AddRow(v.Id,acc, corr, speed, dur,pt,num,hits,subs,ins,del);
                            else
                                table.AddRow(v.Id, acc, corr,speed);
                            no++;
                        }

                        var acct = result.Total.Score == null ? "-" : String.Format("{0:0.00}", 100 * result.Total.Score.Accuracy);
                        var corrt = result.Total.Score == null ? "-" : String.Format("{0:0.00}", 100 * result.Total.Score.Correctness);
                        var speedt = result.Total.Speed == null ? "-" : String.Format("{0:0.00}", result.Total.Speed.Factor);

                        var durt = result.Total.Speed == null ? "-" : TimeSpan.FromTicks((long)result.Total.Speed.StreamDuration).ToString();
                        var ptt = result.Total.Speed == null ? "-" : TimeSpan.FromTicks((long)result.Total.Speed.ProcessingDuration).ToString();

                        var numt = result.Total.Score == null ? "-" : result.Total.Score.Count.ToString();
                        var hitst = result.Total.Score == null ? "-" : result.Total.Score.Hits.ToString();
                        var subst = result.Total.Score == null ? "-" : result.Total.Score.Substitutions.ToString();
                        var inst = result.Total.Score == null ? "-" : result.Total.Score.Insertions.ToString();
                        var delt = result.Total.Score == null ? "-" : result.Total.Score.Deletions.ToString();

                        if (WithDetails)
                            table.AddRow(result.Total.Id, acct, corrt, speedt, durt, ptt, numt, hitst, subst, inst, delt);
                        else
                            table.AddRow(result.Total.Id, acct, corrt,speedt);

                        await output.WriteLineAsync(table.ToString());
                    }
                    break;
                case "diar":
                    using (var output = new StreamWriter(LazyStream.Output(OutputUriOption, "text/plain"), new UTF8Encoding(false)))
                    {
                        var table = new ConsoleTable("Id", "R[%]", "P[%]", "F[%]", "C[%]");
                        if (WithDetails)
                            table = new ConsoleTable("Id", "R[%]", "P[%]", "F[%]", "C[%]", "N", "H", "I", "D","M","C");
                        int no = 0;
                        foreach (var v in result.Items)
                        {
                            var R = v.Dscore == null ? "-" : String.Format("{0:0.00}", 100 * v.Dscore.Recall);
                            var P = v.Dscore == null ? "-" : String.Format("{0:0.00}", 100 * v.Dscore.Precission);

                            var F = v.Dscore == null ? "-" : String.Format("{0:0.00}", 100 * v.Dscore.Frate);
                            var C = v.Dscore == null ? "-" : String.Format("{0:0.00}", 100 * v.Dscore.ClusterPurity);


                            var num = v.Dscore == null ? "-" : v.Dscore.Count.ToString();
                            var hits = v.Dscore == null ? "-" : v.Dscore.Hits.ToString();
                            var ins = v.Dscore == null ? "-" : v.Dscore.Insertions.ToString();
                            var del = v.Dscore == null ? "-" : v.Dscore.Deletions.ToString();
                            var match = v.Dscore == null ? "-" : v.Dscore.ClusterMatch.ToString();
                            var count = v.Dscore == null ? "-" : v.Dscore.ClusterCount.ToString();

                            if (WithDetails)
                                table.AddRow(v.Id, R, P, F, C, num, hits, ins, del,match,count);
                            else
                                table.AddRow(v.Id, R, P, F, C);
                            no++;
                        }
                        {
                            var R = result.Total.Dscore == null ? "-" : String.Format("{0:0.00}", 100 * result.Total.Dscore.Recall);
                            var P = result.Total.Dscore == null ? "-" : String.Format("{0:0.00}", 100 * result.Total.Dscore.Precission);

                            var F = result.Total.Dscore == null ? "-" : String.Format("{0:0.00}", 100 * result.Total.Dscore.Frate);
                            var C = result.Total.Dscore == null ? "-" : String.Format("{0:0.00}", 100 * result.Total.Dscore.ClusterPurity);


                            var num = result.Total.Dscore == null ? "-" : result.Total.Dscore.Count.ToString();
                            var hits = result.Total.Dscore == null ? "-" : result.Total.Dscore.Hits.ToString();
                            var ins = result.Total.Dscore == null ? "-" : result.Total.Dscore.Insertions.ToString();
                            var del = result.Total.Dscore == null ? "-" : result.Total.Dscore.Deletions.ToString();
                            var match = result.Total.Dscore == null ? "-" : result.Total.Dscore.ClusterMatch.ToString();
                            var count = result.Total.Dscore == null ? "-" : result.Total.Dscore.ClusterCount.ToString();

                            if (WithDetails)
                                table.AddRow(result.Total.Id, R, P, F, C, num, hits, ins, del, match, count);
                            else
                                table.AddRow(result.Total.Id, R, P, F, C);
                        }
                        await output.WriteLineAsync(table.ToString());
                    }
                    break;
                case "json":
                    using (var output = new StreamWriter(LazyStream.Output(OutputUriOption, "application/json"), new UTF8Encoding(false)))
                    {
                        await output.WriteLineAsync(result.ToString());
                    }
                    break;
                default:
                    throw new Exception($"Invalid oformat {OutFormat}");
            }

           

            var ret = 0;
            if (result.Total.Score != null && result.Total.Score.Accuracy < MinAccuracy)
            {
                _logger.LogWarning($"Accuracy test failed: {result.Total.Score.Accuracy} < {MinAccuracy}");
                ret += 1;
            }
            if (result.Total.Speed != null && result.Total.Speed.Factor < MinSpeed)
            {
                _logger.LogWarning($"Speed test failed: {result.Total.Speed.Factor} < {MinSpeed}");
                ret += 2;
            }

            return ret;
        }
    }
}