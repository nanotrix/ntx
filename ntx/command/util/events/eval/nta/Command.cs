﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.Threading.Tasks;
using cz.ntx.proto.v2t.misc;
using Dasync.Collections;
using ntx.api.pipe.events;
using ntx.api.util;
using System.Text.RegularExpressions;
using ntx.api;

namespace ntx.command.util.events.eval.nta
{
    class Command : ICommand
    {
        
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "evaluate v2t performance";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             Environment.NewLine
             + "Consumes: nta file format"
             + Environment.NewLine
             + "Produces:  ntx.v2t.misc.Evaluation.Item"
             + Environment.NewLine 
             + Environment.NewLine
             + $"default split: {Constants.DefaultTextSplit}" + Environment.NewLine
             + $"default match: {Constants.DefaultPlusMatch}" + Environment.NewLine;

            var inputUriOption = command.Option("-i|--input",
                "input nta url",
                CommandOptionType.SingleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );

            var blockDuration = command.Option("--maxdur <4500>",
               "max non-match block duration (in ms) inside matching block",
               CommandOptionType.SingleValue
               );
            var splitOption = command.Option($"-s|--split <default>",
                "split regex",
                CommandOptionType.SingleValue
                );

            var plusOption = command.Option(@"-p|--plus <default>",
                "match plus items",
                CommandOptionType.SingleValue
                );
            var idOption = command.Option(@"--id <default>",
                "set evaluation id",
                CommandOptionType.SingleValue
                );
            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    BlockDuration = uint.Parse(blockDuration.GetValueOrDefault()),
                    SplitOption = splitOption.GetValueOrDefault(),
                    PlusOption = plusOption.GetValueOrDefault(),
                    IdOption = idOption.GetValueOrDefault()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private uint BlockDuration { get; set; }
        private string SplitOption { get; set; }
        private string PlusOption { get; set; }
        private string IdOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {

            Regex split = new Regex(SplitOption == "default" ? Constants.DefaultTextSplit : SplitOption);
            Regex plus = new Regex(PlusOption == "default" ? Constants.DefaultPlusMatch : PlusOption);


            var input = LazyStream.Input(InputUriOption, breaker);
            

            var streams=NtaHelper.FetchStreamsFromTar(input, "^events.json|delta.json$");
            if (!streams.ContainsKey("events.json"))
                throw new Exception("Missing events stream");
            if (!streams.ContainsKey("delta.json"))
                throw new Exception("Missing delta stream");

            input.Dispose();
            

            var output = LazyStream.Output(OutputUriOption, "application/json",breaker);







            ulong maxStopReal = ulong.MinValue;
            ulong maxStopStream = ulong.MinValue;


            var eval = new Evaluation.Types.Item { Id = IdOption == "default" ? InputUriOption : IdOption, Speed =new Speed() };

            Event removeRecovery(Event x)
            {

                if (x.BodyCase == Event.BodyOneofCase.Timestamp &&
                 x.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Recovery
                )
                    return null;

                return x;
            }

            var resList = await PipeSource.FromProtoJsonStream<Events>(streams["events.json"], breaker)
                .ViaLookAheadFilter()
                .ViaEventFilter(removeRecovery)
                .ViaInterceptor(x =>
               {

                   if (x.ReceivedAt > maxStopReal)
                       maxStopReal = x.ReceivedAt;
                   return x;
               }
                )
                .ToEventStream()
                .ViaInterceptor(x =>
                {

                    if(x.BodyCase!= Event.BodyOneofCase.Timestamp )
                        return x;
                    
                    switch(x.Timestamp.ValueCase)
                    {
                        case Event.Types.Timestamp.ValueOneofCase.Timestamp_:
                            if (x.Timestamp.Timestamp_ > maxStopStream)
                                maxStopStream = x.Timestamp.Timestamp_;
                            break;
                        case Event.Types.Timestamp.ValueOneofCase.Recovery:
                            if (x.Timestamp.Recovery > maxStopStream)
                                maxStopStream = x.Timestamp.Recovery;
                            break;
                    }

                    return x;
                }
                )
                .ToEventsStream()
                .ViaReLabelAndSplit(split, plus)
                .ViaLabelMerge()
                .ToEventStream()
                .ToListAsync();

            if (maxStopReal != ulong.MinValue)
                eval.Speed.ProcessingDuration = maxStopReal;
            if (maxStopStream != ulong.MinValue)
                eval.Speed.StreamDuration = maxStopStream;



            var reference = (await QuillDeltaContents.FromStream(streams["delta.json"])).ToRawTextStream();
            var refBlockList = await PipeSource.EventsFromRawTextStream(reference, breaker)
                .ViaLookAheadFilter().ViaReLabelAndSplit(split,plus).ViaLabelMerge().ToListAsync();

            var alignment = refBlockList.ToTextList("REF")
            .AlignWith(resList.ToTetxList("RES"))
            .ToAlignBlocks(refBlockList, resList)
            .FineTune(TimeSpan.FromMilliseconds(BlockDuration));


            eval.Blocks.AddRange(alignment);
            eval.GetResults();
            var sink = PipeSink.ProtoJsonStream<Evaluation.Types.Item>(output);
            await sink.WriteAsync(eval);
            await sink.CompleteAsync();
            return 0;
        }
    }
}