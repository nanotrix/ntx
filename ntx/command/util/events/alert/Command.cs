﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.IO;
using ntx.api.pipe.tasks;
using System.Threading.Tasks;
using cz.ntx.proto.v2t.misc;
using ntx.api;
using ntx.api.pipe.events;
using ntx.api.util;
using System.Net.Http;
using Microsoft.Extensions.Logging;

namespace ntx.command.util.events.alert
{

    internal class HttpGetWriter : IAsyncSink<string>
    {
        ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.command.util.events.alert");
        HttpClient client;
        bool nosend = false;
        public HttpGetWriter(bool nosend)
        {
            client =  new HttpClient();
            this.nosend = nosend;
        }
        public async Task WriteAsync(string item)
        {
            _logger.LogInformation($"Calling: {item}");
            try
            {
                if(!nosend)
                    await client.GetAsync(item);
            }
            catch(Exception ex)
            {
                _logger.LogInformation($"Failed: {ex.Message}");
            }
        }
        public Task CompleteAsync()
        {
            return Task.CompletedTask;
        }
    }

    class Command : ICommand
    {

        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "http alert on events";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes: ntx.v2t.engine.Events"
             + Environment.NewLine
             + "Produces: formated http call"
             + Environment.NewLine + Environment.NewLine;
            var inputUriOption = command.Option("-i|--input",
                "input url",
                CommandOptionType.SingleValue
                );
            var outputUriOption = command.Option("-o|--output",
                "output url",
                CommandOptionType.SingleValue
                );
            var labelParamOption = command.Option(@"-l|--label <label>",
                "call get in the form of outputuri?labelParam=label",
                CommandOptionType.SingleValue
                );
            var filterRegex = command.Option(@"-f|--filter <.*>",
             "print only items matching regexp",
             CommandOptionType.SingleValue
             );
            var noSend = command.Option(@"--nosend",
             "don't do http call",
             CommandOptionType.NoValue
             );

            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                outputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    LabelParamOption = labelParamOption.GetValueOrDefault(),
                    FilterParamOption= filterRegex.GetValueOrDefault(),
                    NoSendOption= noSend.HasValue()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private string LabelParamOption { get; set; }
        private string FilterParamOption { get; set; }
        private bool NoSendOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var inputResolver = LazyStream.Input(InputUriOption,breaker);
            var textSink = new HttpGetWriter(NoSendOption);
            await PipeSource.FromProtoJsonStream<Events>(inputResolver, breaker)
                .ViaLookAheadFilter()
                .ToEventStream()
                .ToCompactBlock()
                .ToHttpGetFormat(OutputUriOption,LabelParamOption,FilterParamOption)
                .RunWithSink(textSink);

            return 0;
        }
    }
}