﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.IO;
using ntx.api.pipe.tasks;
using System.Threading.Tasks;
using cz.ntx.proto.v2t.misc;
using ntx.api;
using ntx.api.pipe.events;
using ntx.api.util;

namespace ntx.command.util.events.log
{
    class Command : ICommand
    {

        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "logs events as formated text";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes: ntx.v2t.engine.Events"
             + Environment.NewLine
             + "Produces: formated text TIMESTAMP OFFSET CONFIDENCE TEXT"
             + Environment.NewLine + Environment.NewLine;
            var inputUriOption = command.Option("-i|--input",
                "input url",
                CommandOptionType.SingleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );
            var noiseOption = command.Option(@"-n|--noise",
                "enable nonspeech output",
                CommandOptionType.NoValue
                );
            var liveOption = command.Option(@"-l|--live",
             "print absolute time stamp",
             CommandOptionType.NoValue
             );

            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    NoiseOption = noiseOption.HasValue(),
                    LiveOption=liveOption.HasValue()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private bool NoiseOption { get; set; }
        private bool LiveOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var inputResolver = LazyStream.Input(InputUriOption,breaker);
            var outputResolver = LazyStream.Output(OutputUriOption, "text/plain",breaker);
            var textSink = PipeSink.StringChunkStream(outputResolver);
            await PipeSource.FromProtoJsonStream<Events>(inputResolver, breaker)
                .ViaLookAheadFilter()
                .ToEventStream()
                .ToCompactBlock()
                .ToSimpleLogFormat(NoiseOption, LiveOption)
                .RunWithSink(textSink);

            return 0;
        }
    }
}