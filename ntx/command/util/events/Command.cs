﻿using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using System.Threading.Tasks;

namespace ntx.command.util.events
{
    
    class Command :ICommand
    {
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Command("cat", (c) => cat.Command.Configure(c,options));
            command.Command("conv", (c) => conv.Command.Configure(c, options));
            command.Command("show", (c) => show.Command.Configure(c, options));
            command.Command("tokenize", (c) => tokenize.Command.Configure(c, options));
            command.Command("eval", (c) => eval.Command.Configure(c, options));
            command.Command("merge", (c) => merge.Command.Configure(c, options));
            command.Command("log", (c) => log.Command.Configure(c, options));
            command.Command("alert", (c) => alert.Command.Configure(c, options));
            command.Description = "cli tools for working with ntx.v2t.engine.Events";
            command.OnExecute(() =>
            {
                options.Command = new Command(command);

                return 0;
            });
            
        }
        private readonly CommandLineApplication _app;
        public Command(CommandLineApplication app)
        {
            _app = app;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            await _app.ShowHelpAsync();
            return 1;
        }
    }
}
