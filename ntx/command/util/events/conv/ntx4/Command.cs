﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.IO;
using ntx.api.pipe.tasks;
using System.Threading.Tasks;
using cz.ntx.proto.v2t.misc;
using Dasync.Collections;
using ntx.api.pipe.events;
using System.Text;
using ntx.api.util;
using ConsoleTables;
using Microsoft.Extensions.Logging;
using ntx.api;
using System.Collections.Generic;

namespace ntx.command.util.events.conv.ntx4
{
    class Command : ICommand
    {
       
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.command.util.events.conv.ntx4");
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "converts events to tovek format";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes: ntx.v2t.engine.Events"
             + Environment.NewLine
             + "Produces: ntx4 events"
             + Environment.NewLine + Environment.NewLine;

             var inputUriOption = command.Option("-i|--input",
                "multiple input urls of events file",
                CommandOptionType.MultipleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
             "output url",
             CommandOptionType.SingleValue
             );
            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Values.ToArray(),
                    OutputUriOption = outputUriOption.GetValueOrDefault()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string[] InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var outputResolver = LazyStream.Output(OutputUriOption, "application/json", breaker);
            var textSink = PipeSink.StringChunkStream(outputResolver);
            long counter = 0;
            foreach (var item in InputUriOption)
            {


                using (var inputResolver = LazyStream.Input(item, breaker))
                {
                    await PipeSource.FromProtoJsonStream<Events>(inputResolver, breaker)
                        .ToNTX4Format()
                        .RunWithSink(textSink);
                    counter++;
                }
            }

            return 0;
        }
    }
}