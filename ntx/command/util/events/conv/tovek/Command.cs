﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using System.IO;
using ntx.api.pipe.tasks;
using System.Threading.Tasks;
using cz.ntx.proto.v2t.misc;
using Dasync.Collections;
using ntx.api.pipe.events;
using System.Text;
using ntx.api.util;
using ConsoleTables;
using Microsoft.Extensions.Logging;
using ntx.api;
using System.Collections.Generic;

namespace ntx.command.util.events.conv.tovek
{
    class Command : ICommand
    {
       
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.command.util.events.conv.tovek");
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "converts events to tovek format";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes: ntx.v2t.engine.Events"
             + Environment.NewLine
             + "Produces: tovek confusion network format"
             + Environment.NewLine + Environment.NewLine;

             var inputUriOption = command.Option("-i|--input",
                "multiple input urls of events file",
                CommandOptionType.MultipleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
             "output url",
             CommandOptionType.SingleValue
             );
            var nonSpeechSymbolOption = command.Option("-n|--nonspeech <origin>",
             "default silence symbol, use origin to keep native nonspeech symbols",
             CommandOptionType.SingleValue
             );
            var replaceWhiteSpace = command.Option("-w|--whitespace <origin>",
             "default whitespace symbol in word string, use origin to keep native whitespace symbols",
             CommandOptionType.SingleValue
             );
            var channelMergeStyle = command.Option("-c|--channeladd",
             "change channel merge style form default (<sX></sX>)  to C=X",
             CommandOptionType.NoValue
             );
            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Values.ToArray(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    NonSpeechSymbolOption = nonSpeechSymbolOption.GetValueOrDefault(),
                    WhiteSpaceSymbolOption = replaceWhiteSpace.GetValueOrDefault(),
                    ChannelAddOption = channelMergeStyle.HasValue()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string[] InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private string NonSpeechSymbolOption { get; set; }
        private string WhiteSpaceSymbolOption { get; set; }
        private bool ChannelAddOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var outputResolver = LazyStream.Output(OutputUriOption, "text/plain", breaker);
            var textSink = PipeSink.StringChunkStream(outputResolver);
            long counter = 0;
            foreach (var item in InputUriOption)
            {


                using (var inputResolver = LazyStream.Input(item, breaker))
                {
                    await PipeSource.FromProtoJsonStream<Events>(inputResolver, breaker)
                        .ViaLookAheadFilter()
                        .ViaEventFilter(x=>
                        {
                            switch(x.BodyCase)
                            {
                                case Event.BodyOneofCase.Timestamp:
                                    if (x.Timestamp.ValueCase != Event.Types.Timestamp.ValueOneofCase.Timestamp_)
                                        return null;
                                    return x;
                                case Event.BodyOneofCase.Label:
                                    return x;
                                default:
                                    return null;
                            }
                        }
                        ).ToEventStream()
                        .ViaLabelMerge()
                        .ToCompactBlock()
                        .ToTovekFormat(counter,NonSpeechSymbolOption,WhiteSpaceSymbolOption, ChannelAddOption)
                        .RunWithSink(textSink);
                    counter++;
                }
            }

            return 0;
        }
    }
}