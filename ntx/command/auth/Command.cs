﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using System.Threading.Tasks;

namespace ntx.command.auth
{
    class Command : ICommand
    {
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "authorization related tools";
            command.HelpOption("-h|--help");
            command.Command("info", (c) => info.Command.Configure(c, options));
            command.Command("login", (c) => login.Command.Configure(c, options));
            command.Command("logout", (c) => logout.Command.Configure(c, options));

            command.OnExecute(() =>
            {
                options.Command = new Command(command);
                return 0;
            });
            
        }

        private readonly CommandLineApplication _app;
        public Command(CommandLineApplication app)
        {
            _app = app;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            await _app.ShowHelpAsync();
            return 0;
        }
    }
}
