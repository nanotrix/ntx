﻿using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using System.Threading.Tasks;

namespace ntx.command
{
    
    class Command :ICommand
    {
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Command("auth", (c) => auth.Command.Configure(c,options));
            command.Command("version", (c) => version.Command.Configure(c, options));
            command.Command("task", (c) => task.Command.Configure(c, options));
            command.Command("util", (c) => util.Command.Configure(c, options));
            command.Command("devel", (c) => devel.Command.Configure(c, options));

            command.OnExecute(() =>
            {
                options.Command = new Command(command);

                return 0;
            });
            
        }
        private readonly CommandLineApplication _app;
        public Command(CommandLineApplication app)
        {
            _app = app;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            await _app.ShowHelpAsync();
            return 1;
        }


    }
}
