#!/bin/bash
cp -r . /build
cd /build/test
dotnet restore
dotnet test
cd /work